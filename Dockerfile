# Copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research
FROM python:3.6

# TODO gebruik requirements file, update ook in gitlab-ci.yml
RUN pip install bigchaindb_driver==0.6.2 cryptography python-gnupg Pillow markdown django==2.2.6 django-tinymce django-filter djangorestframework==3.10.3 django-cors-headers rdflib rdflib-jsonld coreapi pyyaml pymongo
COPY app /usr/app
WORKDIR /usr/app
CMD [ "python", "manage.py", "runserver", "0.0.0.0:8000"]