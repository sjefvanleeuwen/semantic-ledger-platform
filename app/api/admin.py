# Copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research
from django.contrib import admin

from api.models import CryptoId, CoreAssets, PgpKey


class CryptoIdAdmin(admin.ModelAdmin):
    list_display = ('user', 'alias', 'public_key')
    list_filter = ['user']
    readonly_fields = ('public_key', 'secure_received_public', 'alias', 'user')


class CoreShapeAdmin(admin.ModelAdmin):
    list_display = ('alias', 'asset_id')
    list_filter = ['alias']
    readonly_fields = ('alias', 'asset_id')


class PgpAdmin(admin.ModelAdmin):
    list_display = ('crypto_id', 'public_b64', 'private_b64', 'passphrase')
    list_filter = ('crypto_id',)


admin.site.register(CryptoId, CryptoIdAdmin)
admin.site.register(CoreAssets, CoreShapeAdmin)
admin.site.register(PgpKey, PgpAdmin)
