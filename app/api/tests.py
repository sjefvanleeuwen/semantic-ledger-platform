# Copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research
import os
import json
from django.urls import reverse
from django.contrib.auth.models import User
from rest_framework.test import APITestCase
from rest_framework.test import APIClient
from rest_framework.authtoken.models import Token
from rest_framework import status

class CryptoTestCase(APITestCase):
    def setUp(self):
        # setup user and token
        self.admin = User.objects.create_superuser('testuser', '', 'hallo123')
        token = Token.objects.get(user__username='testuser')
        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        print("Token: ", token.key)

    def create_crypto_id(self, alias):
        # create crypto_id
        url = reverse('crypto_id_list')
        data = {
            "alias": alias
        }
        response = self.client.post(url, data, format='json')

        if response.status_code != status.HTTP_200_OK:
            print("ERR:", response.data)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        crypto_id = response.data
        print("\ncrypto id (", alias, "):", crypto_id['keypair']['public_key'])

        return {
            'public_key': crypto_id['keypair']['public_key'],
            'private_key':  crypto_id['keypair']['private_key']
        }

    def add_pgp(self, crypto_id, private_key, pgp=None):
        url = reverse('pgp')

        postdata = {
            'crypto_id': crypto_id,
            'private_key': private_key
        }

        if pgp:
            postdata['pgp_public'] = pgp['pgp_public']
            postdata['pgp_private'] = pgp['pgp_private']
            postdata['passphrase'] = pgp['passphrase']

        response = self.client.post(url, data=postdata)
        if response.status_code != status.HTTP_200_OK:
            print("ERR:", response.data)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        print("New PGP for %s: tx: %s" % (crypto_id, response.data['tx_id']))
        return response.data

    def get_assets(self, alias):
        # GET my assets
        url = reverse('get_assets', args=[alias])
        response = self.client.get(url)
        if response.status_code != status.HTTP_200_OK:
            print("ERR:", response.data)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        
        return response.data

    def get_semantic_asset(self, asset_id):
        url = reverse('get_semantic_asset', args=[asset_id])
        response = self.client.get(url)
        if response.status_code != status.HTTP_200_OK:
            print("ERR:", response.data)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        return response.data

    def get_rdf_asset(self, asset_id):
        url = reverse('get_rdf', args=[asset_id])
        response = self.client.get(url)
        if response.status_code != status.HTTP_200_OK:
            print("ERR:", response.data)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        return response.data

    def print_assets(self, alias, print_content=None):
        assets = self.get_assets(alias)
        print("\nAssets of %s: %s" % (alias, assets))

        if print_content:
            for asset in assets:
                if print_content == 'asset':
                    # print("\nTypes: %s\n%s" % (asset['types'], self.get_semantic_asset(asset['asset_id'])))
                    print("(Deprecated statement about types)")
                elif print_content == 'rdf':
                    print("\nRDF: %s\n" % self.get_rdf_asset(asset['asset_id']))
        return assets

    def retrieve_secure_assets(self, assets, alias, private_key):
        # Secure assets of alias
        secure_assets = [asset['asset_id'] for asset in assets if 'secure_payload_unlocker' in asset['types']]
        for secure_asset in secure_assets:
            url = reverse('retrieve_secure_publication')

            postdata = {
                'asset_id': secure_asset,
                'private_key': private_key
            }

            response = self.client.post(url, data=postdata)
            if response.status_code != status.HTTP_200_OK:
                print("ERROR: %s" % response.data)
            self.assertEqual(response.status_code, status.HTTP_200_OK)

            print("\n%s retrieved secure-asset tx: %s" % (alias, response.data))

    def save_publication_alias(self, crypto_id, asset_id, alias):
        url = reverse('add_alias')

        postdata = {
            'crypto_id': crypto_id,
            'asset_id': asset_id,
            'alias': alias
        }
        response = self.client.post(url, data=postdata)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(response.data)

    def validate_assets(self, assets):
        for asset in assets:
            url = reverse('validate_asset', args=[asset['asset_id']])

            response = self.client.get(url)
            if response.status_code == status.HTTP_200_OK:
                print("Validation of %s: %s" % (asset, response.data))
            self.assertIn(response.status_code, (status.HTTP_200_OK, status.HTTP_404_NOT_FOUND,))

    def read_pgp_key(self, public_file, private_file):
        public_location = "{}{}".format(os.path.dirname(os.path.realpath(__file__)), public_file)
        private_location = "{}{}".format(os.path.dirname(os.path.realpath(__file__)), private_file)

        with open(public_location) as f:
            public_key = f.read()
            f.close()

        with open(private_location) as f:
            private_key = f.read()
            f.close()

        return {
            "pgp_public": public_key,
            "pgp_private": private_key
        }


class SemanticPlatformTest(CryptoTestCase):
    def test_publication(self):
        keys_folder = '/testdata/keys/'

        alias_alice = "Alice"
        alias_bob = "Bob"
        alias_carol = "Carol"

        # read PGP key files
        public_key_format = "{}{}_public.asc"
        private_key_format = "{}{}_private.asc"

        # Alice
        pgp_alice = self.read_pgp_key(
            public_key_format.format(keys_folder, alias_alice),
            private_key_format.format(keys_folder, alias_alice),
        )
        pgp_alice['passphrase'] = 'halloalice'

        # Bob
        pgp_bob = self.read_pgp_key(
            public_key_format.format(keys_folder, alias_bob),
            private_key_format.format(keys_folder, alias_bob),
        )
        pgp_bob['passphrase'] = 'hallobob'

        # Carol
        pgp_carol = self.read_pgp_key(
            public_key_format.format(keys_folder, alias_carol),
            private_key_format.format(keys_folder, alias_carol),
        )
        pgp_carol['passphrase'] = 'hallocarol'

        # 1: Create crypto-id for alice
        alice = self.create_crypto_id(alias_alice)

        # 2: Publish all shapes with alice
        shapes = [
            {
                'file': '/shapes/PGP_Shape.ttl',
                'asset_alias': 'PGP_shacl'
            },
            {
                'file': '/shapes/Unlocker_Shape.ttl',
                'asset_alias': 'Unlocker_shacl'
            },
            {
                'file': '/shapes/Mapping_Shape.ttl',
                'asset_alias': 'Mapping_shacl'
            },
        ]

        shape_assets = {}
        for shape in shapes:
            file_location = "{}{}".format(os.path.dirname(os.path.realpath(__file__)), shape['file'])

            # load file
            with open(file_location) as f:
                shape_content = f.read()
                f.close()

            # See if file was read.
            self.assertTrue(shape_content)
            postdata = {
                'crypto_id': alias_alice,
                'private_key': alice['private_key'],
                'publication': shape_content,
                'format': 'n3',
                'asset_alias': shape['asset_alias']
            }

            url = reverse('add_publication')
            response = self.client.post(url, data=postdata)
            if response.status_code != status.HTTP_200_OK:
                print("ERR:", response.data)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            shape_assets[shape['asset_alias']] = response.data
            print("Shape (%s): %s" % (shape['asset_alias'], shape_assets[shape['asset_alias']]))

            url = reverse('core_shapes')
            postdata = {
                'alias': shape['asset_alias'],
                'asset_id': response.data
            }
            response = self.client.post(url, data=postdata)
            if response.status_code != status.HTTP_200_OK:
                print("ERR:", response.data)
            self.assertEqual(response.status_code, status.HTTP_200_OK)

        # verify core shapes
        url = reverse('core_shapes')
        response = self.client.get(url)
        if response.status_code != status.HTTP_200_OK:
            print("ERR:", response.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        print("Core assets: %s" % response.data)

        # 3: Create PGP for alice
        self.add_pgp(alias_alice, alice['private_key'], pgp_alice)

        # 4: Create other ID's
        bob = self.create_crypto_id(alias_bob)
        self.add_pgp(alias_bob, bob['private_key'], pgp_bob)

        carol = self.create_crypto_id(alias_carol)
        self.add_pgp(alias_carol, carol['private_key'], pgp_carol)

        # Load files with test-data
        invalid_data_file = "{}{}".format(os.path.dirname(os.path.realpath(__file__)), '/testdata/testdata_invalid.json')
        valid_data_file = "{}{}".format(os.path.dirname(os.path.realpath(__file__)), '/testdata/testdata_valid.json')
        shape_file = "{}{}".format(os.path.dirname(os.path.realpath(__file__)), '/testdata/rules.ttl')
        with open(invalid_data_file) as f:
            invalid_jsonld_str = f.read()
            f.close()

        with open(valid_data_file) as f:
            valid_jsonld_str = f.read()
            f.close()

        with open(shape_file) as f:
            shape_str = f.read()
            f.close()

        recipients = [
            {'recipients': (bob['public_key'],),
             'amount': 123},
        ]

        postdata = {
            'crypto_id': alias_alice,
            'private_key': alice['private_key'],
            'publication': invalid_jsonld_str,
            'shapes': [
                {
                    'shape': shape_str,
                    'shape_format': 'n3',
                }
            ],
            'recipients': recipients,
            # 'recipients': recipients,
            'secure': True
        }

        # Publish invalid data
        url = reverse('add_publication')
        response = self.client.post(url, data=postdata, format='json')

        # Validation report
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        print("\nData is invalid: Validation report (simple): %s" % response.data)

        # Publish valid data
        postdata['publication'] = valid_jsonld_str
        response = self.client.post(url, data=postdata)
        if response.status_code != status.HTTP_200_OK:
            print("ERR:", response.data)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        print("\nData is valid, publish from alice to bob! (%s)" % response.data)

        # Assets of Alice
        self.print_assets(alias_alice, 'asset')

        # Assets of Bob
        assets_bob = self.print_assets(alias_bob)

        # Secure assets of Bob
        self.retrieve_secure_assets(assets_bob, alias_bob, bob['private_key'])

        # Assets of Bob
        assets_bob = self.print_assets(alias_bob)

        # Semantic assets of Alice, Bob and Carol
        self.print_assets(alias_alice)

        # Secure assets of bob
        secure_assets_bob = [asset['asset_id'] for asset in assets_bob if 'secure_payload_unlocker' in asset['types']]
        print("\nSecure semantic assets of %s: %s" % (alias_bob, secure_assets_bob))

        # Transfer secure assets from Bob to Carol
        for secure_asset in secure_assets_bob:
            url = reverse('transfer_asset')

            recipients = [
                {'recipients': (carol['public_key'],),
                 'amount': 123},
            ]

            postdata = {
                'asset_id': secure_asset,
                'crypto_id': alias_bob,
                'private_key': bob['private_key'],
                'recipients': recipients,
                'secure': True
            }
            response = self.client.post(url, data=postdata)
            print(response.data)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            print("\nAsset %s transferred from %s to %s" % (secure_asset, alias_bob, alias_carol))

        # Get semantic assets of Bob and Carol
        self.print_assets(alias_bob)
        assets_carol = self.print_assets(alias_carol)

        # Retrieve secure assets of Carol
        self.retrieve_secure_assets(assets_carol, alias_carol, carol['private_key'])

        # Assets
        assets_alice = self.print_assets(alias_alice, print_content='rdf')
        self.print_assets(alias_bob, print_content='rdf')
        self.print_assets(alias_carol, print_content='rdf')

        # Validate assets
        print("\nValidate assets of ALICE")
        self.validate_assets(assets_alice)

        print("\nValidate assets of CAROL")
        self.validate_assets(assets_carol)
