# Copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research
import base64
from rest_framework import serializers

from django.contrib.auth.models import User
from api.models import CryptoId, CoreAssets, PgpKey
from api.crypto.crypto import Crypto

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'password', 'first_name', 'last_name', 'email', 'is_superuser', 'is_staff', 'is_active', 'last_login',)
        write_only_fields = ('password',)
        read_only_fields = ('date_joined', 'last_login')

    def create(self, validated_data):
        u = User(username=validated_data['username'])

        u.first_name = validated_data.get('first_name', "")
        u.last_name = validated_data.get('last_name', "")
        u.email = validated_data.get('email', "")
        u.is_staff = validated_data.get('is_staff', False)
        u.is_superuser = validated_data.get('is_superuser', False)
        u.is_active = validated_data.get('is_active', True)

        u.set_password(validated_data['password'])
        u.save()
        return u

    def update(self, instance, validated_data):
        instance.username = validated_data.get('username', instance.username)
        instance.first_name = validated_data.get('first_name', instance.first_name)
        instance.last_name = validated_data.get('last_name', instance.last_name)
        instance.email = validated_data.get('email', instance.email)
        instance.is_staff = validated_data.get('is_staff', instance.is_staff)
        instance.is_superuser = validated_data.get('is_superuser', instance.is_superuser)
        instance.is_active = validated_data.get('is_active', instance.is_active)

        if 'password' in validated_data:
            instance.set_password(validated_data['password'])
        instance.save()
        return instance


class CryptoIdSerializer(serializers.ModelSerializer):
    class Meta:
        model = CryptoId
        fields = ('user', 'alias', 'public_key', 'secure_received_public')
        write_only_fields = ('user',)


class PgpSerializer(serializers.ModelSerializer):
    id_private_key = serializers.CharField(max_length=44, required=True)
    pgp_public = serializers.CharField(required=False)
    pgp_private = serializers.CharField(required=False)
    pgp_passphrase = serializers.CharField(required=False)

    class Meta:
        model = PgpKey
        fields = ('crypto_id', 'passphrase')
        write_only_fields = ('private_b64', 'passphrase')

    def create(self, validated_data):
        # Get from input, or generate
        if 'pgp_public' in validated_data and 'pgp_private' in validated_data and 'pgp_passphrase' in validated_data:
            pgp = {
                'public': base64.b64encode(validated_data['pgp_public'].encode()).decode(),
                'private': base64.b64encode(validated_data['pgp_private'].encode()).decode(),
                'passphrase': validated_data['pgp_passphrase']
            }
        else:
            pgp = Crypto.gpg_create_key(
                validated_data['crypto_id'],
                "{}@{}".format(validated_data['crypto_id'], "bdb"),
                validated_data['id_private_key']
            )

        key = PgpKey(
            crypto_id=validated_data['crypto_id'],
            passphrase=pgp['passphrase'],
            public_b64=pgp['public'],
            private_b64=pgp['private']
        )
        key.save()
        return key


class PgpInputSerializer(serializers.Serializer):
    crypto_id = serializers.CharField(max_length=50, required=True)
    private_key = serializers.CharField(max_length=44, required=True)
    pgp_public = serializers.CharField(required=False)
    pgp_private = serializers.CharField(required=False)
    passphrase = serializers.CharField(required=False)


class AssetAliasSerializer(serializers.Serializer):
    crypto_id = serializers.CharField(max_length=50, required=True)
    asset_id = serializers.CharField(required=True)
    alias = serializers.CharField(max_length=50, required=True)


class RecipientSerializer(serializers.Serializer):
    recipients = serializers.ListField(
        required=True,
        min_length=1,
        child=serializers.CharField(max_length=44)
    )
    amount = serializers.IntegerField(required=False, min_value=1)
    threshold = serializers.IntegerField(required=False, min_value=1)


class ShapeSerializer(serializers.Serializer):
    shape = serializers.CharField(required=True)
    shape_format = serializers.CharField(required=True)


class PublicationInputSerializer(serializers.Serializer):
    crypto_id = serializers.CharField(max_length=50, required=True)
    private_key = serializers.CharField(max_length=44, required=True)
    publication = serializers.CharField(required=True)
    format = serializers.CharField(default='json-ld')
    # TODO: Split secure in "Private" (Hide sender of asset) and "Confidential" (Encrypted asset)
    secure = serializers.BooleanField(required=False, default=False)
    shapes = ShapeSerializer(many=True, required=False)
    recipients = RecipientSerializer(many=True, required=False)
    amount = serializers.IntegerField(default=1)
    asset_alias = serializers.CharField(required=False)


class TransferInputSerializer(serializers.Serializer):
    crypto_id = serializers.CharField(max_length=50, required=True)
    private_key = serializers.CharField(max_length=44, required=True)
    asset_id = serializers.CharField(required=True)
    recipients = RecipientSerializer(many=True, required=False)
    secure = serializers.BooleanField(default=False)
    metadata = serializers.JSONField(required=False)

class RetrieveSecureAssetSerializer(serializers.Serializer):
    asset_id = serializers.CharField(required=True)
    private_key = serializers.CharField(max_length=44, required=True)


class CoreAssetSerializer(serializers.ModelSerializer):
    class Meta:
        model = CoreAssets
        fields = ('alias', 'asset_id')
