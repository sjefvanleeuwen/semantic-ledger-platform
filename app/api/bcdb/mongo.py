# Copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research
"""
A module for direct communication to MongoDB.
"""

from pymongo import MongoClient

class Mongo:
    # TODO this depends on the name of the mongo service in
    # docker-compose; pass an ENV for this...
    client = MongoClient('mongodb://mongo:27017/')
    db = client.bigchain
    
    @classmethod
    def getInputs(cls, public_key, filter_operation=None):
        """
        Get the inputs of all transactions where
        some user @public_key was a previous owner.

        @filter_operation {'CREATE'|'TRANSFER'}:
        Return only inputs if the transaction performed
        this operation.
        """
        #TODO optionally, filter on transaction mode (create/transfer)
        
        txs = cls.db.transactions.find()

        # Construct return object
        result = []
        for tx in txs:
            for index, input in enumerate(tx['inputs']):
                # Check that the user was mentioned in the input
                if public_key in input['owners_before']:
                    # Check that the tx is of the relevant type
                    if filter_operation and tx['operation'] in filter_operation:
                        result.append({'input_index':index, 'transaction_id':tx['id']})

        return result