# Copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research
import os
from bigchaindb_driver import BigchainDB
from bigchaindb_driver.crypto import generate_keypair
from bigchaindb_driver.exceptions import NotFoundError
from cryptoconditions import Ed25519Sha256, ThresholdSha256
import base58
import base64
import json
import re
from sha3 import sha3_256
from rdflib.namespace import RDF

from api.semantics.secure_semantics import SecureSemantics
from api.crypto.crypto import Crypto

from api.bcdb.mongo import Mongo

# The URL for the BigchainDB ledger API
# TODO: Make this a list of urls (nodes)
BDB_URL = os.getenv('BDB_URL', 'http://localhost:9984')

# How to reference internal BDB assets
BDB_URL_FORMAT = "bdb://{}/"


class Bdb:
    # Constructor: Set parameters
    def __init__(self, url=BDB_URL):
        self.bdb = BigchainDB(url)
        self.NODE_NS = os.getenv('NODE_NS', 'http://localhost:8000/api/resource/')
        self.bdb_asset_pattern = r"^(bdb\:\/\/)(?P<asset_id>[a-zA-Z0-9]+)\/$"

    # Generate BigchainDB Keypair
    @staticmethod
    def generate_keypair():
        return generate_keypair()

    # Prepare single output of a transaction:
    # Who is the recipient (can be a list, if new ownership is shared - multisig tx -)
    # of an asset (and what is the amount to receive)
    def prepare_output(self, recipients, amount, threshold):
        # If this is a multisig tx, what is the threshold for this ownership
        if len(recipients) > 1:
            print("Detected multiple recipients, creating threshold condition")
            if not threshold:
                threshold = len(recipients)

            threshold_sha256 = ThresholdSha256(threshold=threshold)

            for r in recipients:
                print("Adding subfulfillment for {}".format(r))
                threshold_sha256.add_subfulfillment(Ed25519Sha256(public_key=base58.b58decode(r)))

            # Create condition object
            condition_details = {
                'subconditions': [
                    {
                        'type': s['body'].TYPE_NAME,
                        'public_key': base58.b58encode(s['body'].public_key).decode()
                    }
                    for s in threshold_sha256.subconditions
                    if (s['type'] == 'fulfillment' and
                        s['body'].TYPE_NAME == 'ed25519-sha-256')
                ],
                'threshold': threshold_sha256.threshold,
                'type': threshold_sha256.TYPE_NAME,
            }
            print("Created condition details: \n{}".format(str(condition_details)))

            condition_uri = threshold_sha256.condition.serialize_uri()
        else:
            # Single recipient: Simple public-key condition.
            r_ed25519 = Ed25519Sha256(public_key=base58.b58decode(recipients[0]))
            condition_details = {
                'type': r_ed25519.TYPE_NAME,
                'public_key': base58.b58encode(r_ed25519.public_key).decode()
            }
            condition_uri = r_ed25519.condition.serialize_uri()

        # Output (destination)
        output = {
            'amount': str(amount),
            'condition': {
                'details': condition_details,
                'uri': condition_uri,
            },
            'public_keys': tuple([r for r in recipients]),
        }
        # print("output:\n", output)
        return output

    # A transaction can have multiple recipients (which can be complex in itself). Each recipient needs an output
    def prepare_outputs(self, recipients):
        outputs = []

        for recipient in recipients:
            # If amount is not set, set amount to '1'
            if not ('amount' in recipient):
                recipient['amount'] = 1

            # If threshold is not set, set to number of recipients
            if not ('threshold' in recipient):
                recipient['threshold'] = len(recipient['recipients'])

            # Generate output and append to outputs
            outputs.append(self.prepare_output(recipient['recipients'], recipient['amount'], recipient['threshold']))

        return outputs

    # Int overflow fix
    def check_field_value(self, value):
        if type(value) == int:
            if value > 2**63-1 or value < 0 - 2**63:
                raise ValueError("INT value too large, SLP supports upto 8 byte(signed) integers")

    # Part of INT overflow fix
    def check_asset_struct(self, asset):
        if type(asset) == dict:
            for key in asset.keys():
                if type(asset[key]) == list:
                    for item in asset[key]:
                        self.check_asset_struct(item)
                else:
                    self.check_asset_struct(asset[key])
        elif type(asset) == list:
            for item in asset:
                self.check_asset_struct(item)
        else:
            self.check_field_value(asset)


    # Helper function for signing Ed25519:
    @classmethod
    def signEd25519(cls, message, signatory, senderKeys):
        """
        Helper function for signing Ed25519.

        @message is the message being signed for
        @signatory is the public key of a crypto identity
        that should sign it
        @senderKeys is a dictionary of pubkey:privkey
        of all the senders of the message.

        The method is written as a helper for prepare_tx.
        It checks for the case where the signatory is not
        among the senders, which can occur in m-of-n threshold
        conditions. If the signatory is not found, a ValueError
        is raised.

        If everything goes well, the function returns a
        signed signature object.
        """
        # Check if the signatory is among the senders
        if signatory in senderKeys:
            pubKey = signatory
            privKey = senderKeys[signatory]
            # Generate a Signature object
            s_ed25519 = Ed25519Sha256(public_key=base58.b58decode(pubKey))
            s_ed25519.sign(message.digest(), base58.b58decode(privKey))
            return s_ed25519
        else:
            # Signatory was not found
            raise ValueError("Senders are unable to sign for {}".format(
                signatory
                )
            )

    @classmethod
    def fulfillCreate(cls, senders, tx):
        """
        Create fulfillments for all inputs of a
        CREATE transaction.

        @tx is the unfulfilled transaction.
        The method adds the fulfillments
        into the tx dictionary and then returns it.

        @senders is a list of crypto IDs that are
        sending the message; they provide the
        necessary signatures.

        Typically this method will be called by
        prepare_tx, so argument validation is
        performed there.
        """
        # Create JSON object
        json_tx = json.dumps(
            tx,
            sort_keys=True,
            separators=(',', ':'),
            ensure_ascii=False,
        )
        # Loop over inputs, creating a fulfillment for each
        for i, _ in enumerate(tx['inputs']):
            # sha3 message
            message = sha3_256(json_tx.encode())
            # if there are multiple senders, fulfill threshold condition
            if len(senders) > 1:
                # Multiple senders - default to a 1-of-n threshold condition
                threshold_sha256_senders = ThresholdSha256(threshold=1)
                # Sign tx by each sender
                for s in senders:
                    s_ed25519 = Ed25519Sha256(public_key=base58.b58decode(s.public_key))
                    s_ed25519.sign(message.digest(),
                        private_key=base58.b58decode(s.private_key))
                    threshold_sha256_senders.add_subfulfillment(s_ed25519)
                # Generate uri
                fulfillment_uri = threshold_sha256_senders.serialize_uri()
            elif len(senders) == 1:
                # One sender -- simply sign an Ed25519
                s_ed25519 = Ed25519Sha256(public_key=base58.b58decode(senders[0].public_key))
                s_ed25519.sign(message.digest(), base58.b58decode(senders[0].private_key))
                fulfillment_uri = s_ed25519.serialize_uri()

            # update input with uri
            tx['inputs'][i]['fulfillment'] = fulfillment_uri
        # Return the tx with fulfillments
        return tx

    def fulfillTransfer(self, senders, tx):
        """
        Method to create fulfillments for
        TRANSFER transactions.

        Differs from fulfillCreate() in its handling
        of threshold conditions in the outputs
        of the preceding transaction.

        Since this method typically gets called from
        prepare_tx(), validation of function arguments
        is performed there.
        """        
        # Create JSON object
        json_tx = json.dumps(
            tx,
            sort_keys=True,
            separators=(',', ':'),
            ensure_ascii=False,
        )
        # Cast senders to a convenient format
        senderKeys = {s.public_key : s.private_key for s in senders}
        # Debug
        print("Fulfilling transfer tx that now has inputs: {}".format(tx['inputs']))
        # Loop over inputs
        for i, _ in enumerate(tx['inputs']):
            # Generate sha3 message
            message = sha3_256(json_tx.encode())
            # Update message with txID and output index
            txID = tx['inputs'][i]['fulfills']['transaction_id']
            outputIndex = tx['inputs'][i]['fulfills']['output_index']
            update_str = '{}{}'.format(txID, outputIndex)
            message.update(update_str.encode())
            # Get the output and its type
            try:
                outputTx = self.bdb.transactions.retrieve(txID)
            except Exception as e:
                raise RuntimeError("Could not retrieve transaction with ID {}: {}".format(txID, str(e)))
            output = outputTx['outputs'][outputIndex]
            outputType = output['condition']['details']['type']
            # Handle the output based on type
            if outputType == 'ed25519-sha-256':
                # Sign it
                signature = self.signEd25519(message, output['condition']['details']['public_key'], senderKeys)
                fulfillment_uri = signature.serialize_uri()
            elif outputType == 'threshold-sha-256':
                # Sign each of the subconditions
                # NOTE This method assumes there are no nested thresholds,
                # so all the subconditions must be Ed25519.
                # Create threshold signature object
                threshold = output['condition']['details']['threshold']
                thresholdSignature = ThresholdSha256(threshold=threshold)
                signatureCount = 0
                # Collect signatures for each of the subconditions
                for subcondition in output['condition']['details']['subconditions']:
                    # Get the public key associated with this subcondition
                    signatory = subcondition['public_key']
                    try:
                        # Try to sign with this public key
                        # If the signatory is not among the senders,
                        # this will raise ValueError
                        signature = self.signEd25519(message, signatory, senderKeys)
                        thresholdSignature.add_subfulfillment(signature)
                        signatureCount += 1
                    except ValueError:
                        # No harm done, in case of m-of-n we
                        # might not need all the signatures.
                        # But instead, a subcondition is needed.
                        # Inspired by https://github.com/bigchaindb/bigchaindb-driver/blob/6f962524c4b458d6e002c080b7168a9e92c5f67a/docs/handcraft.rst#multiple-owners-with-m-of-n-signatures
                        thresholdSignature.add_subcondition(
                            Ed25519Sha256(public_key=base58.b58decode(signatory)).condition
                        )
                # Check that enough signatures were found
                if signatureCount < threshold:
                    raise ValueError("Not enough signatures found to fulfill output {} of tx {}".format(
                    outputIndex, txID
                ))
                # Generate the URI
                fulfillment_uri = thresholdSignature.serialize_uri()
            else:
                # Unknown signature type
                raise ValueError("Signature type {} not supported".format(outputType))
            # update input with uri
            tx['inputs'][i]['fulfillment'] = fulfillment_uri
        # Return fulfilled transaction
        return tx

    # Prepare transaction before sending it: Create TX structure
    def prepare_tx(self, senders, operation, asset, metadata, outputs, inputs):
        """
        Create dictionaries containing all the needed fields for bigchaindb to
        accept a transaction.

        This method is largely inspired by Bigchaindb tutorials on
        "handcrafting" transactions, e.g.
        http://docs.bigchaindb.com/projects/py-driver/en/latest/handcraft.html

        Method does NOT support nested inputs/outputs.
        Method does support threshold signatures.
        """
        # Catch any INTs that are too large
        self.check_asset_struct(asset)
        # Make sure there is at least one sender
        try:
            if len(senders) == 0 or isinstance(senders, str):
                raise ValueError("Senders must be a non-empty collection")
        except TypeError as e:
            raise ValueError("Encountered '{}' error. Senders must be a non-empty collection".format(str(e)))
        # Check that the operation type is supported
        if not operation in ["CREATE", "TRANSFER"]:
            raise ValueError("Operation type '{}' not recognized".format(operation))

        # create transaction object
        version = '2.0'
        tx = {
            'operation': operation,
            'asset': asset,
            'metadata': metadata,
            'outputs': outputs,
            'inputs': inputs,
            'version': version,
            'id': None,
        }
        # Create fulfillments for the inputs.
        # TRANSFER operations require more complex
        # processing, so the method flow splits here
        # depending on operation type.
        if operation == "CREATE":
            tx = self.fulfillCreate(senders, tx)
        elif operation == "TRANSFER":
            tx = self.fulfillTransfer(senders, tx)

        # generate TX id
        tx['id'] = sha3_256(json.dumps(
            tx,
            sort_keys=True,
            separators=(',', ':'),
            ensure_ascii=False,
        ).encode()).hexdigest()

        # Debug
        # Print tx. Skip the asset because it contains the data, which might be large strings.
        debugdict = {key:value for key, value in tx.items() if key != 'asset'}
        # Extract the asset id for transfer operations
        if 'asset' in tx and 'id' in tx['asset']:
            debugdict['assetID'] = tx['asset']['id']
        print("Prepared tx: {}".format(debugdict))
        # Force print
        import sys
        sys.stdout.flush()


        return tx

    # PART OF CONFIDENTIAL TX FRAMEWORK
    # Create anonymous keypairs for recipients of confidential TX (to receive encrypted payload in)
    # And anonymous keypairs for senders to send TX from
    def create_anonymous_keypairs(self, recipients, senders, current_owner=None):
        # Dict to map recipients to anonymous_recipients
        deposit_boxes = []
        # Anonymous Recipients to send encrypted tokens to
        anonymous_recipients = []
        for r in recipients:
            anonymous_recipient = {
                'recipients': [],
                'amount': r['amount']
            }
            deposit_box = {
                'signers': [],
                'amount': r['amount']
            }
            for signer in r['recipients']:
                keypair = generate_keypair()
                anonymous_recipient['recipients'].append(keypair.public_key)
                signer = {
                    'recipient': signer,
                    'keypair': keypair
                }
                deposit_box['signers'].append(signer)
            anonymous_recipients.append(anonymous_recipient)
            deposit_boxes.append(deposit_box)

        # Anonymous senders to send encrypted tokens from
        anonymous_senders = []
        for s in senders:
            # create mapping sender->anonymous_keypair
            sender_mapping = {
                'anonymous_unlocker_sender': generate_keypair(),
                'sender': s
            }

            # only generate anonymouse payload-sender for CREATE transaction
            if not current_owner:
                sender_mapping['anonymous_payload_sender'] = generate_keypair()
            else:
                sender_mapping['anonymous_payload_sender'] = current_owner

            anonymous_senders.append(sender_mapping)

        return {
            'deposit_boxes': deposit_boxes,
            'anonymous_recipients': anonymous_recipients,
            'anonymous_senders': anonymous_senders
        }

    # PART OF CONFIDENTIAL TX FRAMEWORK
    def sender_mappings(self, anonymous_senders, mapping_shape):
        secure_semantics = SecureSemantics()
        crypto = Crypto()

        # publish keypair-mappings
        sender_mappings = []
        for s in anonymous_senders:
            # generate mapping RDF
            mapping_rdf = secure_semantics.create_anonymous_sender_mapping(s)

            # Encrypt RDF
            sender_pgp = self.get_available_pgp(s['sender'].public_key)
            if not sender_pgp:
                raise Exception("No valid PGP found")

            # Save as asset on-chain
            mapping = {
                'rdf': mapping_rdf,
                'constraints': [mapping_shape]
            }

            # Get shape ID
            shape_asset_id = re.match(self.bdb_asset_pattern, mapping_shape).group('asset_id')
            if not shape_asset_id:
               raise AttributeError('Invalid shape reference')

            # Get shape
            shape = self.get_asset(shape_asset_id)['rdf']

            # Validate RDF against shape
            mapping_valid = secure_semantics.validate_rdf(mapping_rdf, shape)
            if not mapping_valid['conforms']:
                raise AttributeError('RDF does not match shape')

            mapping_payload = {
                'secure_mapping': crypto.gpg_encrypt(
                    sender_pgp,
                    json.dumps(mapping)
                )
            }

            sender_mappings.append(self.create_plain(
                mapping_payload,
                (s['sender'],),
                [{'recipients': [s['sender'].public_key]}]
            ))
        return sender_mappings

    # PART OF CONFIDENTIAL TX FRAMEWORK
    def publish_unlockers(self, secure_txid, unlocker_shape, deposit_boxes, anonymous_senders, symmetric_key):
        secure_semantics = SecureSemantics()
        # publish unlockers
        unlockers = []
        for deposit_box in deposit_boxes:
            for signer in deposit_box['signers']:
                # Get public key from recipient
                signer_key = self.get_available_pgp(signer['recipient'])

                signatures = Crypto.sign_ed25519(secure_txid.encode(), list(s['sender'] for s in anonymous_senders))

                rdf = secure_semantics.create_secure_msg_unlocker(
                    signer['keypair'],
                    symmetric_key,
                    secure_txid,
                    signatures
                )

                # add shape
                unlocker_publication = {
                    'rdf': rdf,
                    'constraints': [unlocker_shape]
                }

                # Get shape ID
                shape_asset_id = re.match(self.bdb_asset_pattern, unlocker_shape).group('asset_id')
                if not shape_asset_id:
                    raise AttributeError('Invalid shape reference')

                # Get shape
                shape = self.get_asset(shape_asset_id)['rdf']

                # Validate RDF against shape
                rdf_valid = secure_semantics.validate_rdf(rdf, shape)
                if not rdf_valid['conforms']:
                    raise AttributeError('RDF does not match shape')

                # Encrypt details for recipient, using signer_key
                b64_cipher = Crypto.gpg_encrypt(signer_key, json.dumps(unlocker_publication))

                # send to signer
                unlocker_payload = {
                    'secure_payload_unlocker': b64_cipher
                }

                unlocker_recipients = [
                    {'recipients': (signer['recipient'],),
                     'amount': 1},
                ]

                # Save unlocker to ledger
                unlocker_tx = self.create_plain(
                    unlocker_payload,
                    tuple([sender['anonymous_unlocker_sender'] for sender in anonymous_senders]),
                    unlocker_recipients
                )
                unlockers.append(unlocker_tx['id'])
        return unlockers

    # Create TX
    def create(self, payload, senders, recipients, metadata=None, secure=False, secure_publish_shape_assets=None):
        if secure:
            return self.secure_create(
                payload=payload,
                senders=senders,
                recipients=recipients,
                secure_publish_shape_assets=secure_publish_shape_assets,
                metadata=metadata
            )
        else:
            return self.create_plain(
                payload=payload,
                senders=senders,
                recipients=recipients,
                metadata=metadata
            )

    # Create plain, unencrypted/not confidential transaction
    def create_plain(self, payload, senders, recipients, metadata=None):
        outputs = self.prepare_outputs(recipients)

        # To be fulfilled input (sender(s)):
        input_ = {
            'fulfillment': None,
            'fulfills': None,
            'owners_before': tuple([s.public_key for s in senders])
        }

        asset = {
            'data': payload
        }

        tx = self.prepare_tx(senders, 'CREATE', asset, metadata, outputs, (input_,))

        returned_tx = self.bdb.transactions.send_commit(tx)

        return {
            'id': returned_tx['id']
        }

    # Create Secure TX: Encrypted and confidential
    def secure_create(self, payload, senders, recipients, secure_publish_shape_assets, metadata=None):
        # Create proxy keypair to send confidential transaction
        anon_keypairs = self.create_anonymous_keypairs(recipients, senders)

        # Generate symmetric key to encrypt payload
        sym_key = Crypto.get_new_symmetric_key()

        # Encrypt payload
        payload = {
            'secure_payload': Crypto.fernet_encrypt(sym_key, json.dumps(payload)),
        }

        # publish secure (encrypted) payload
        secure_create = self.create_plain(
            payload,
            tuple([anon_sender['anonymous_payload_sender'] for anon_sender in anon_keypairs['anonymous_senders']]),
            anon_keypairs['anonymous_recipients'], metadata
        )

        # publish keypair-mappings
        sender_mappings = self.sender_mappings(anon_keypairs['anonymous_senders'], secure_publish_shape_assets['mapping_shape_id'])

        # publish unlockers
        unlockers = self.publish_unlockers(
            deposit_boxes=anon_keypairs['deposit_boxes'],
            symmetric_key=sym_key.decode(),
            secure_txid=secure_create['id'],
            anonymous_senders=anon_keypairs['anonymous_senders'],
            unlocker_shape=secure_publish_shape_assets['unlocker_shape_id']
        )

        secure_create['unlockers'] = unlockers
        secure_create['sender_mappings'] = sender_mappings
        secure_create['deposit_boxes'] = anon_keypairs['deposit_boxes']
        secure_create['secure_payload'] = payload['secure_payload']
        return secure_create

    # Get Transaction from the ledger
    def get_tx(self, tx_id):
        try:
            tx = self.bdb.transactions.retrieve(tx_id)
        except NotFoundError:
            raise AttributeError("Transaction not found")
        return tx

    # Get assets current owners of asset (= unspent outputs)
    def get_asset_uspent_outputs(self, asset_id):
        asset_txs = self.bdb.transactions.get(asset_id=asset_id)

        # get outputs
        outputs = []
        for asset_tx in asset_txs:
            for idx, output in enumerate(asset_tx['outputs']):
                outputs.append({
                    'output': {
                        'transaction_id': asset_tx['id'],
                        'output_index': idx,
                    },
                    'public_keys': output['public_keys'],
                    'amount': int(output['amount'])
                })

        # see if outputs are spent
        spent_outputs = []
        for asset_tx in asset_txs:
            if asset_tx['operation'] == 'TRANSFER':
                for inp in asset_tx['inputs']:
                    for idx, op in enumerate(outputs):
                        if inp['fulfills']['transaction_id'] == op['output']['transaction_id'] and inp['fulfills']['output_index'] == op['output']['output_index']:
                            spent_outputs.append(idx)

        for spent_idx in sorted(spent_outputs, reverse=True):
            del outputs[spent_idx]
        return outputs

    # prepare inputs for total amount/senders combination
    def prepare_inputs(self, asset_id, senders, total_amount):
        # Debug
        print("Preparing inputs for assetID {}, given senders as {} and a target total_amount of {}".format(asset_id, senders, total_amount))
        inputs = []

        # Determine whether one or multiple inputs are needed (can tx be fulfilled with one input?)
        # get asset transaction ids
        asset_tx_ids = [tx['id'] for tx in self.bdb.transactions.get(asset_id=asset_id)]
        # initiate empty arrays
        exact_outputs, larger_outputs, smaller_outputs = [], [], []
        for sender in senders:
            # get sender unspent outputs
            sender_unspent_outputs = self.bdb.outputs.get(sender.public_key, spent=False)
            # Debug
            print("Sender {} has the following unspent_outputs: {}".format(str(sender), sender_unspent_outputs))

            # loop over unspent outputs, to see if they are large enough (amount >= total amount) and match senders
            for sender_unspent_output in sender_unspent_outputs:
                # if this output is linked to asset_id...
                if sender_unspent_output['transaction_id'] in asset_tx_ids:
                    # get output object
                    output = self.bdb.transactions.retrieve(sender_unspent_output['transaction_id'])['outputs'][sender_unspent_output['output_index']]

                    # check if output public_keys matches senders
                    sender_matches_count = 0
                    # get public keys of senders
                    sender_pubkeys = [s.public_key for s in senders]
                    # for each sender pubkey
                    for sender_pubkey in sender_pubkeys:
                        if sender_pubkey in output['public_keys']:
                            # increment match-count
                            sender_matches_count = sender_matches_count + 1
                        else:
                            # invalid sender
                            raise Exception("Invalid sender found")
                    
                    # Debug
                    print("Determined {} senders matches for output {}".format(sender_matches_count, output))

                    # See if number of senders matches required amount
                    if (
                            # Single owner, 1+ matches are fine
                            output['condition']['details']["type"] == "ed25519-sha-256" and
                            sender_matches_count
                    ) or ( # Multiple owners, must reach threshold value
                            output['condition']['details']["type"] == "threshold-sha-256" and
                            sender_matches_count >= output['condition']['details']["threshold"]
                    ):
                        # get output amount
                        amount = int(output['amount'])
                        # put output in right array
                        if amount == total_amount:
                            exact_outputs.append(sender_unspent_output)
                        elif amount > total_amount:
                            larger_outputs.append(sender_unspent_output)
                        elif amount < total_amount:
                            smaller_outputs.append(sender_unspent_output)
                        # Debug
                        print("Output {} with amount {} registered as relevant output".format(output, amount))

        # simplest case, one output for input
        if len(exact_outputs) or len(larger_outputs):
            # Single input needed
            # take first output
            output = exact_outputs[0] if len(exact_outputs) else larger_outputs[0]
            # Debug
            print("Inputs can suffice with single output to append: {}".format(output))
            inputs.append({
                'fulfillment': None,
                'fulfills': {
                    'transaction_id': output['transaction_id'],
                    'output_index': output['output_index']
                },
                'owners_before': tuple(
                    self.bdb.transactions.retrieve(txid=output['transaction_id'])['outputs'][output['output_index']]['public_keys']
                )
            })
        elif len(smaller_outputs):
            # Multiple inputs are needed
            # Set input-size to 0
            total_input_size = 0
            # Debug
            print("Constructing inputs through smaller outputs: {}".format(smaller_outputs))
            # Loop over the outputs
            for o in smaller_outputs:
                # See if cumulative input-size is large enough, if not
                if total_input_size < total_amount:
                    # get output data
                    output = self.bdb.transactions.retrieve(o['transaction_id'])['outputs'][o['output_index']]

                    # append output to inputs
                    inputs.append({
                        'fulfillment': None,
                        'fulfills': {
                            'transaction_id': o['transaction_id'],
                            'output_index': o['output_index']
                        },
                        'owners_before': tuple(
                            output['public_keys']
                        )
                    })
                    # Debug
                    print("Appended output {}".format(output))

                    # increase cumulative amount
                    total_input_size = total_input_size + int(output['amount'])

        # Debug
        print("Done building inputs (length {}): {}".format(len(inputs), inputs))
        # Force print
        import sys
        sys.stdout.flush()
        return tuple(inputs)

    def transfer_secure(self, unlocker_asset_id, sender, sender_pgps, recipients, secure_publish_shape_assets, metadata=None):
        # get unlocker asset from ledger
        unlocker_asset = self.get_asset(unlocker_asset_id)

        # verify asset-type
        if not (unlocker_asset and 'secure_payload_unlocker' in unlocker_asset):
            print("Asset not a secure_payload_unlocker")
            return False

        # Get secure_payload asset linked to the unlocker
        # Decrypt unlocker asset
        secure_asset_unlocker = None
        for pgp_key in sender_pgps:
            if not secure_asset_unlocker:
                secure_asset_unlocker = Crypto.gpg_decrypt(
                    pgp_key.private_b64,
                    pgp_key.passphrase,
                    unlocker_asset['secure_payload_unlocker']
                )
                if secure_asset_unlocker:
                    secure_asset_unlocker = json.loads(secure_asset_unlocker)

        if not secure_asset_unlocker:
            raise ValueError("Unable to decrypt secure_payload_unlocker")

        # Validate unlocker
        secure_semantics = SecureSemantics()

        try:
            unlocker = secure_semantics.get_unlocker(secure_asset_unlocker['rdf'])
        except ValueError as err:
            raise err

        # Get amount
        sender_balance = self.get_asset_balance(unlocker.crypto_id.public_key, unlocker.asset_id)

        # Verify amount with total to-send
        amount_to_send = 0
        for r in recipients:
            amount_to_send = amount_to_send + r['amount']

        if amount_to_send > sender_balance:
            print("Insufficient balance (%s - %s) %s < %s" % (unlocker.crypto_id.public_key,
                                                              unlocker.asset_id,
                                                              sender_balance,
                                                              amount_to_send)
                  )
            return False

        # Generate anonymous keypairs
        anon_keypairs = self.create_anonymous_keypairs(
            recipients=recipients,
            senders=(sender,),
            current_owner=unlocker.crypto_id
        )

        # publish secure payload
        transfer_tx = {
            'id': self.transfer_plain(
                asset_id=unlocker.asset_id,
                senders=(unlocker.crypto_id,),
                recipients=anon_keypairs['anonymous_recipients'],
                metadata=metadata
            )
        }

        # publish unlockers
        unlockers = self.publish_unlockers(
            deposit_boxes=anon_keypairs['deposit_boxes'],
            symmetric_key=unlocker.symmetric_key,
            secure_txid=unlocker.asset_id,
            anonymous_senders=anon_keypairs['anonymous_senders'],
            unlocker_shape=secure_publish_shape_assets['unlocker_shape_id']
        )

        # publish keypair-mappings
        sender_mappings = self.sender_mappings(anon_keypairs['anonymous_senders'], secure_publish_shape_assets['mapping_shape_id'])

        # Transfer used unlockers (unlocker_asset) to sender.received
        used_unlocker_recipient = [{
            'recipients': (sender.secure_received_public,),
            'amount': self.get_asset_balance(sender.public_key, unlocker_asset_id)
        }]

        used_unlocker_tx = self.transfer_plain(unlocker_asset_id, (sender,), used_unlocker_recipient)

        transfer_tx['unlockers'] = unlockers
        transfer_tx['sender_mappings'] = sender_mappings
        transfer_tx['deposit_boxes'] = anon_keypairs['deposit_boxes']
        transfer_tx['secure_asset'] = unlocker.asset_id
        return transfer_tx

    def transfer_plain(self, asset_id, senders, recipients, metadata=None):
        total_amount = 0
        for r in recipients:
            total_amount = total_amount + r['amount']
        inputs = self.prepare_inputs(asset_id, senders, total_amount)

        # add recipient if inputs are larger than total_amount
        input_total = 0
        for i in inputs:
            output = self.bdb.transactions.retrieve(i['fulfills']['transaction_id'])['outputs'][i['fulfills']['output_index']]
            input_total = input_total + int(output['amount'])

        # in input is too high, transfer rest back to sender(s)
        if input_total > total_amount:
            recipients.append({
                'recipients': tuple([s.public_key for s in senders]),
                'amount': input_total - total_amount
            })
        # Generate tx outputs
        outputs = self.prepare_outputs(recipients)
        try:
            # Prepare full tx object
            tx = self.prepare_tx(
                senders=senders,
                operation='TRANSFER',
                asset={'id': asset_id},
                metadata=metadata,
                outputs=outputs,
                inputs=inputs
            )
        except (ValueError, KeyError, TypeError) as e:
            print("Could not prepare transfer tx, msg: {}".format(str(e)))
            raise RuntimeError(e)

        # Debug
        print("Sending transfer tx to BDB")
        # Send tx to bdb
        returned_tx = self.bdb.transactions.send_commit(tx)
        return returned_tx['id']

    def get_ledger_status(self):
        try:
            return self.bdb.info()
        except:
            return False

    def get_asset_balance(self, owner, asset_id):
        assets = self.get_assets(owner)
        balance = 0
        for ownedAssetID, assetDict in assets.items():
            # print("%s: %s" % (a['asset_id'], a['amount']))
            if ownedAssetID == asset_id:
                balance = balance + assetDict['amount']
        return balance

    def get_asset(self, asset_id):
        try:
            tx = self.bdb.transactions.retrieve(asset_id)
            if tx['operation'] == 'CREATE':
                return tx['asset']['data']
            else:
                return False
        except:
            return False

    def get_assets(self, owner, spent=False):
        """
        Returns an overview of assets currently owned by @owner.
        @owner is a public key value.

        The return object is structured as a dictionary,
        where asset IDs function as keys, and the values are
        dictionaries containing several pieces of information
        about the asset.

        {'assetID1':
            {'output':<the unspent output of the owner>,
             'asset':<the asset, stripped down to its 'data' value>,
             'amount':<the amount of the asset held>
            }
        }

        NOTE This function ignores the case of multiple unspent outputs
        of the same owner. This means amounts are unreliable,
        as are outputs. The upside is that the return object is
        somewhat simplified.
        TODO consider refactoring to a simple {assetID, assetData}
        return object.
        """
        # Crucially, spent=False so that only unspent outputs
        # (i.e. 'owned' assets) are considered.
        try:
            owner_outputs = self.bdb.outputs.get(owner, spent=spent)
        except Exception as e:
            raise RuntimeError("Could not retrieve outputs from BDB: {}".format(str(e)))

        assets = {}
        for output in owner_outputs:
            try:
                txID = output['transaction_id']
                tx = self.bdb.transactions.retrieve(txID)
            except Exception as e:
                print("Could not retrieve tx at {}: {}".format(txID, str(e)))
                continue
            if tx['operation'] == 'CREATE':
                asset_tx = tx
            elif tx['operation'] == 'TRANSFER':
                asset_tx = self.bdb.transactions.retrieve(tx['asset']['id'])

            assets[asset_tx['id']] = {
                'output': output,
                'asset': asset_tx['asset']['data'],
                'amount': int(tx['outputs'][output['output_index']]['amount'])
            }
        return assets

    
    def get_transactions_of_user(self, public_key, spent=None, created=False):
        """
        Retrieve all transactions that have an output containing @public_key.

        If @spent is set to True or False, this restricts the query to
        spent or unspent outputs, respectively.
        If @created is set to True, this will only return assets
        co-created by the public key's holder.

        Relevant BDB documentation on the outputs model:
        http://docs.bigchaindb.com/projects/server/en/v2.0.0b9/http-client-server-api.html#transaction-outputs

        The format of the return data is a dictionary
        that groups an asset and all its relevant transactions by the asset ID:
        {asset_id1:
            { 'asset': asset,
              'transactions': [tx1, tx2]
            },
         asset_id2:{...}
        }
        """
        # TODO deal with TimeOutError? http://docs.bigchaindb.com/projects/py-driver/en/latest/_modules/bigchaindb_driver/transport.html
        # TODO deal with None returns? Or do we assume we can (still) trust self.bdb.outputs.get ?
        
        # Set up return data object
        tx_dict = {}

        # Set up collector list
        # for both inputs and outputs.
        # Structured as list of tuples:
        # [(asset_id, tx_id, tx), ...]
        tx_list = []

        # Retrieve inputs.
        # Background: assets that are transferred upon creation
        # might not identify the creator in an output.
        # So we add CREATE txs where a user was in the input.
        inputs = Mongo.getInputs(public_key, filter_operation='CREATE')

        # Retrieve all outputs
        # Not necessary if @created is True, since in that case
        # we are only looking for txs the user created.
        if created:
            outputs = []
        else:
            outputs = self.bdb.outputs.get(public_key, spent=spent)

        # Construct tuples from both inputs and outputs
        for inoutput in inputs+outputs:
            # Extract tx ID and asset ID information from the outputs
            tx_id = inoutput['transaction_id']
            tx = self.bdb.transactions.retrieve(tx_id)
            if tx['operation'] == 'CREATE':
                asset_id = tx_id
            elif tx['operation'] == 'TRANSFER':
                asset_id = tx['asset']['id']
            tx_list.append(
                (asset_id, tx_id, tx)
            )

        # Loop over collected tuples
        # to construct return dict
        # This groups transactions by asset
        for asset_id, tx_id, tx in tx_list:
            # Check if the dictionary key exists
            if not asset_id in tx_dict:
                # Initialize the dictionary entry
                # Create the transactions list
                tx_dict[asset_id] = {'transactions':[]}
                # Attach the asset separately for convenience
                tx_dict[asset_id]['asset'] = tx['asset']

            # If the key already exists,
            # simply add the transaction to the list
            tx_dict[asset_id]['transactions'].append(tx)
        
        return tx_dict

    def get_transactions_of_asset(self, asset_id, sort=False):
        """
        Wrapper function to retrieve txs for an asset.
        http://docs.bigchaindb.com/projects/server/en/v2.0.0b9/http-client-server-api.html#transactions

        The @sort argument indicates whether the assets should
        be returned in chronological order.
        """
        # Define the sort helper function
        def sortTransactions(txs):
            # We will build up a dictionary of ID orderings
            # And a quick reference to the asset by id
            txOrder = {}
            assets = {}
            for tx in txs:
                # Store the asset by ID
                assets[tx['id']] = tx
                # Get the preceding tx...
                if tx['operation'] == "CREATE":
                    # No previous tx, this gets the special key None
                    txOrder[None] = tx['id']
                elif tx['operation'] == "TRANSFER":
                    # Get the previous txID from the inputs
                    # TODO this might not work for txs that bundle
                    # inputs from multiple other txs... but we don't use that currently.
                    oldTxID = tx['inputs'][0]['fulfills']['transaction_id']
                    newTxID = tx['id']
                    txOrder[oldTxID] = newTxID
                else:
                    #Unknown operation type
                    raise ValueError("Operation type {} not supported".format(tx['operation']))
            # Use the ordering dictionary to put the assets in the right order.
            # Create collector return object
            sortedTxs = []
            # Start from the CREATE ID
            currentID = txOrder[None]
            # Recurse over ordering pairs
            while True:
                # Add to ordered list of txs
                sortedTxs.append(assets[currentID])
                # Move to next txID in order,
                # or terminate if there is no more ordering information
                if currentID in txOrder:
                    currentID = txOrder[currentID]
                else:
                    break
            return sortedTxs

        txs = self.bdb.transactions.get(asset_id=asset_id)
        if sort:
            txs = sortTransactions(txs)
        return txs

    # Returns first available PGP key of pubkey
    def get_available_pgp(self, owner):
        assets = self.get_assets(owner)

        if not assets:
            return False
        else:
            secure_semantics = SecureSemantics()
            for assetID, assetDict in assets.items():
                if 'rdf' in assetDict['asset']:
                    data_asset = assetDict['asset']['rdf']

                    if secure_semantics.get_matching_triples(
                            data_asset,
                            subject=SecureSemantics.return_uri_ref(secure_semantics.NODE_ID),
                            predicate=RDF.type,
                            object_=SecureSemantics.SECURE_ASSETS.PGP_Public
                    ):
                        v = secure_semantics.get_matching_triples(
                            data_asset,
                            subject=secure_semantics.return_uri_ref(SecureSemantics.NODE_ID),
                            predicate=SecureSemantics.SECURE_ASSETS.public_key
                        )
                        if len(v):
                            return base64.b64decode(v[0][2]).decode()

    def get_asset_ids(self, owner):
        assets = self.get_assets(owner)

        if not assets:
            return False
        else:
            # Else, extract the asset ID and
            # 'types'... NOTE not sure types is still
            # working / useful?
            semantic_assets = []
            for assetID, assetDict in assets.items():
                semantic_assets.append({
                    'asset_id': assetID,
                    'types': tuple(assetDict['asset'].keys())
                })
            return semantic_assets

    def get_block_height(self, tx_id):
        """
        This is a wrapper function for the block height getter endpoint:
        http://docs.bigchaindb.com/projects/server/en/v2.0.0b9/http-client-server-api.html#blocks
        """
        return self.bdb.blocks.get(txid=tx_id)

    def get_locked_assets(self, owner):
        assets = self.get_assets(owner)

        if not assets:
            return False
        else:
            locked_assets = []
            for assetID, assetDict in assets.items():
                for key in assetDict['asset'].keys():
                    if key == 'secure_payload':
                        locked_assets.append(assetDict['asset_id'])
            return locked_assets

    def retrieve_secure_asset(self, asset_id, deposit_crypto_id, symmetric_key, unlocker_asset_id, crypto_id, unlocker_shape):
        # create new deposit_crypto_id
        new_deposit_crypto_id = generate_keypair()

        # define recipients of asset
        recipients = [{
            'recipients': (new_deposit_crypto_id.public_key,),
            'amount': self.get_asset_balance(deposit_crypto_id.public_key, asset_id)
        }]

        # transfer secure_asset (full amount) to this deposit_crypto_id,
        self.transfer_plain(asset_id, (deposit_crypto_id,), recipients)

        # create new unlocker asset (encrypted: new_deposit_crypto_id, and transfer_txid) for crypto_id
        # Create RDF document with details
        secure_semantics = SecureSemantics()

        signatures = Crypto.sign_ed25519(asset_id.encode(), [crypto_id])

        rdf = secure_semantics.create_secure_msg_unlocker(
            new_deposit_crypto_id,
            symmetric_key,
            asset_id,
            signatures
        )

        # add shape
        unlocker_publication = {
            'rdf': rdf,
            'constraints': [unlocker_shape]
        }

        # Get shape ID
        shape_asset_id = re.match(self.bdb_asset_pattern, unlocker_shape).group('asset_id')
        if not shape_asset_id:
            raise AttributeError('Invalid shape reference')

        # Get shape
        shape = self.get_asset(shape_asset_id)['rdf']

        # Validate RDF against shape
        rdf_valid = secure_semantics.validate_rdf(rdf, shape)
        if not rdf_valid['conforms']:
            raise AttributeError('RDF does not match shape')

        # Get PGP key
        signer_key = self.get_available_pgp(crypto_id.public_key)

        # Encrypt details for recipient, using signer_key
        b64_cipher = Crypto.gpg_encrypt(signer_key, json.dumps(unlocker_publication))

        # send to signer
        unlocker_payload = {
            'secure_payload_unlocker': b64_cipher
        }

        unlocker_recipients = [
            {'recipients': (crypto_id.public_key,),
             'amount': 1},
        ]

        # transfer secure_payload_unlocker to used_unlocker_id for this crypto_id
        unlocker_tx = self.create(unlocker_payload, (crypto_id,), unlocker_recipients)

        # Transfer 'used' unlocker asset-id to 'used' account of crypto-id
        used_unlocker_recipient = [{
            'recipients': (crypto_id.secure_received_public,),
            'amount': self.get_asset_balance(crypto_id.public_key, unlocker_asset_id)
        }]

        self.transfer_plain(unlocker_asset_id, (crypto_id,), used_unlocker_recipient)
        return unlocker_tx
