# Copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research
from cryptography.fernet import Fernet
import gnupg
import base58
import base64
from cryptoconditions import Ed25519Sha256


class Crypto:
    @staticmethod
    def get_new_symmetric_key():
        return Fernet.generate_key()

    @staticmethod
    def fernet_encrypt(key, data):
        return Fernet(key).encrypt(bytes(data, 'utf-8')).decode('utf-8')

    @staticmethod
    def fernet_decrypt(key, cipher):
        return Fernet(key).decrypt(bytes(cipher, 'utf-8')).decode('utf-8')

    @staticmethod
    def sign_ed25519(input_, signers):
        signatures = []
        for s in signers:
            s_ed25519 = Ed25519Sha256(public_key=base58.b58decode(s.public_key))
            signature = s_ed25519.sign(input_, private_key=base58.b58decode(s.private_key))

            signatures.append({
                'sender': s.public_key,
                'signature': base64.b64encode(signature).decode()
            })
        return signatures

    @staticmethod
    def verify_ed25519(signature, input_, public_key):
        v_ed25519 = Ed25519Sha256(public_key=base58.b58decode(public_key))
        v_ed25519.signature = base64.b64decode(signature)
        return v_ed25519.validate(message=input_)

    @staticmethod
    def gpg_encrypt(public_keys, data, sign_fingerprint=''):
        # multiple, or just one public key
        if not isinstance(public_keys, str):
            public_keys_str = "\r\n".join(public_keys)
            pass
        else:
            public_keys_str = public_keys

        gpg = gnupg.GPG()
        imported = gpg.import_keys(public_keys_str)

        # if successfully imported
        if imported.count > 0:
            # get stored keys
            stored_keys = gpg.list_keys()
            # find the key
            encrypt_for = []
            for key in stored_keys:
                if key['fingerprint'] in imported.fingerprints:
                    encrypt_for.append(key['fingerprint'])

            # encrypt
            if len(encrypt_for):
                if sign_fingerprint is not '':
                    # encrypt and sign data
                    cipher = gpg.encrypt(data, encrypt_for, always_trust=True, sign=sign_fingerprint)
                else:
                    # encrypt data
                    cipher = gpg.encrypt(data, encrypt_for, always_trust=True)
                # verify successful encryption
                if cipher.ok:
                    # TODO: remove public keys from keystore
                    b64_cipher = base64.b64encode(str(cipher).encode()).decode()
                    return b64_cipher
                else:
                    return False

    @staticmethod
    def gpg_decrypt(private_key_b64, passphrase, data):
        gpg = gnupg.GPG()

        gpg.import_keys(base64.b64decode(private_key_b64).decode())

        decrypted = gpg.decrypt(base64.b64decode(data).decode(), passphrase=passphrase, always_trust=True)
        # TODO: Remove private key
        if decrypted.ok:
            return str(decrypted)
        else:
            return False

    @staticmethod
    def gpg_create_key(name, email, passphrase):
        gpg = gnupg.GPG()
        gpg.encoding = 'utf-8'
        key_input = gpg.gen_key_input(key_type="RSA", key_length="2048",
                                      passphrase=passphrase,
                                      name_email=email,
                                      name_real=name)

        pgp_key = gpg.gen_key(key_input)
        b64_public_key = base64.b64encode(gpg.export_keys(pgp_key.fingerprint).encode()).decode()
        b64_private_key = base64.b64encode(gpg.export_keys(pgp_key.fingerprint,
                                                           secret=True,
                                                           passphrase=passphrase).encode()).decode()

        return {
            "public": b64_public_key,
            "private": b64_private_key,
            "passphrase": passphrase
        }
