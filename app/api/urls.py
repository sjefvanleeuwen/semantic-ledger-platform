# Copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research
from django.urls import path, re_path
from django.views.generic import TemplateView

from rest_framework.schemas import get_schema_view

from api.views import UserViews, \
    CryptoIdViews, \
    BdbViews, \
    PgpViews, \
    PublicationsViews, \
    AssetViews, \
    CoreShapeViews, \
    TokenViews, \
    TransactionViews

urlpatterns = [
    re_path(r'^auth_token/$', TokenViews.CreateTokenViews.as_view(), name='create_token'),
    re_path(r'^auth_token/(?P<username>[0-9a-zA-Z]+)/(?P<password>[a-z0-9A-Z_\-:]+)$', TokenViews.TokenViews.as_view(),
            name='get_token'),
    path('openapi/', get_schema_view(
        title="SLT Platform API",
        description="Semantic Ledger Technology: API documentation page",
        version="1.0.0",
    ), name='openapi-schema'),
    path('docs/', TemplateView.as_view(
        template_name='swagger-ui.html',
        extra_context={'schema_url': 'openapi-schema'}
    ), name='swagger-ui'),

    # re_path(r'^user/(?P<username>[0-9a-zA-Z]+)?$', UserViews.AdminView.as_view(), name='user_view'),
    re_path(r'^user/$', UserViews.UserListView.as_view(), name='user_view'),
    re_path(r'^user/(?P<username>[0-9a-zA-Z]+)$', UserViews.UserDetailView.as_view(), name='user_view'),

    path('ledger_status/', BdbViews.LedgerStatus.as_view(), name='ledger_status'),

    # Crypto-id routes
    # re_path(r'^id/(?P<crypto_id>[0-9a-zA-Z:\.\-\_]+)?$', CryptoIdViews.CryptoIdView.as_view(), name='crypto_id'),
    re_path(r'^id/$', CryptoIdViews.CryptoIdListView.as_view(), name='crypto_id_list'),
    re_path(r'^id/(?P<alias>[a-zA-Z0-9\-_:\.]+)$', CryptoIdViews.CryptoIdDetail.as_view(), name='crypto_id_detail'),

    # PGP routes
    path('pgp/', PgpViews.AddPgpAsset.as_view(), name='pgp'),

    # Route for (secure) publication
    path('publish/', PublicationsViews.PublicationsView.as_view(), name='add_publication'),

    # Route to save publication_alias
    re_path(r'^publication_alias/(?P<alias>.+)?$', PublicationsViews.AssetAliasView.as_view(), name='add_alias'),

    # Route for transfer of asset
    path('transfer/', PublicationsViews.TransferAssetView.as_view(), name='transfer_asset'),

    # Route for retrieving (semantic) asset owned by alias
    re_path(r'^assets/(?P<crypto_id>[0-9a-zA-Z_\-:\.]+)/$', AssetViews.GetAssetView.as_view(),
            name='get_assets'),

    # Route for getting specific asset
    re_path(r'^asset/(?P<asset_id>[0-9a-zA-Z]+)/$', AssetViews.AssetView.as_view(),
            name='get_asset'),

    # Route for getting specific semantic asset
    re_path(r'^semantic_asset/(?P<asset_id>[0-9a-zA-Z]+)/$', AssetViews.AssetView.as_view(),
            name='get_semantic_asset'),

    # Get publication as RDF
    re_path(r'^rdf/(?P<asset_id>[0-9a-zA-Z]+)/(?P<rdf_node>.+)?$', AssetViews.RdfAssetView.as_view(),
            name='get_rdf'),

    # Retrieve secure publication (make your own)
    re_path(r'^retrieve_secure_publication/$', PublicationsViews.SecureAssetView.as_view(),
            name='retrieve_secure_publication'),

    # Validate asset
    re_path(r'^validate_asset/(?P<asset_id>[0-9a-zA-Z]+)/$', AssetViews.ValidatorViews.as_view(),
            name='validate_asset'),

    # Asset unspent outputs
    re_path(r'^unspent/(?P<asset_id>[0-9a-zA-Z]+)/(?P<crypto_id>[0-9a-zA-Z]+)?$', AssetViews.AssetUnspentView.as_view(),
            name='unspent'),

    # History: retrieve all outputs relating to a user or asset
    re_path(r'^history/(?P<crypto_id>[0-9a-zA-Z_\-:\.]+)$', TransactionViews.TransactionHistoryView.as_view(), name='slpid-history'),
    re_path(r'^asset/(?P<asset_id>[0-9a-zA-Z]+)/transactions/$', TransactionViews.FromAssetView.as_view(),
            name='get_asset_transactions'),

    # Transaction views
    re_path(r'^transaction/(?P<tx_id>[0-9a-zA-Z]+)(/)$', TransactionViews.FullTransactionView.as_view()),
    re_path(r'^transaction/inputs/(?P<tx_id>[0-9a-zA-Z]+)$', TransactionViews.TransactionInputsView.as_view()),
    re_path(r'^transaction/outputs/(?P<tx_id>[0-9a-zA-Z]+)$', TransactionViews.TransactionOutputsView.as_view()),

    # Outputs views
    re_path(r'^id/(?P<alias>[0-9a-zA-Z_\-:\.]+)/outputs/?$', CryptoIdViews.OutputView.as_view(), name='get-outputs-id'),
    re_path(r'^id/(?P<pubkey>[0-9a-zA-Z_\-:\.]+)/outputs-pubkey/?$', CryptoIdViews.OutputPubkeyView.as_view(), name='get-outputs-pubkey'),
    
    # Core shapes
    re_path(r'^core_shapes/$', CoreShapeViews.CoreShapeListViews.as_view(), name='core_shapes'),
    re_path(r'^core_shapes/(?P<alias>[0-9a-zA-Z_\-:\.]+)$', CoreShapeViews.CoreShapeDetailView.as_view(), name='core_details'),
]
