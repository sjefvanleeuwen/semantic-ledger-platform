# Copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research
from django.db import models
from django.contrib.auth import models as auth_models
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


# BigchainDB crypto-id
class CryptoId(models.Model):
    user = models.ForeignKey(auth_models.User, on_delete=models.DO_NOTHING, null=False, blank=False)
    alias = models.CharField(max_length=50, null=False, blank=False)
    public_key = models.CharField(max_length=44, null=False, blank=False)
    secure_received_public = models.CharField(max_length=44, null=False, blank=False)

    class Meta:
        unique_together = ('user', 'alias')
        app_label = "api"

    def __str__(self):
        return "{}.{}".format(self.user.username, self.alias)


class PgpKey(models.Model):
    crypto_id = models.ForeignKey(CryptoId, on_delete=models.DO_NOTHING, null=False, blank=False)
    public_b64 = models.TextField(null=False, blank=False, unique=True)
    private_b64 = models.TextField(null=False, blank=False, unique=True)
    passphrase = models.TextField(null=False, blank=False, unique=True)

    class Meta:
        app_label = "api"

    def __str__(self):
        return self.public_b64


class AssetReference(models.Model):
    crypto_id = models.ForeignKey(CryptoId, on_delete=models.DO_NOTHING, null=False, blank=False)
    alias = models.CharField(max_length=255, null=False, blank=False)
    asset_id = models.CharField(max_length=255, null=False, blank=False)

    class Meta:
        unique_together = ('crypto_id', 'alias')
        app_label = "api"

    def __str__(self):
        return self.alias


class CoreAssets(models.Model):
    alias = models.CharField(max_length=255, null=False, blank=False, unique=True)
    asset_id = models.CharField(max_length=255, null=False, blank=False)

    class Meta:
        app_label = "api"

    def __str__(self):
        return self.alias
