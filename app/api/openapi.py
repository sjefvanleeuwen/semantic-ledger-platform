# Copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research
from rest_framework.schemas.openapi import AutoSchema


class Ssschema(AutoSchema):
    def get_operation(self, path, method):
        operation = super().get_operation(path, method)
        return operation
