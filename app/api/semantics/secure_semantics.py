# Copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research
import rdflib
import os
import requests
from rest_framework import status
from rdflib.namespace import RDF, RDFS
import json
import re
from collections import namedtuple

from api.crypto.crypto import Crypto

BDB_NS = "bdb://"


class SecureSemantics:
    secure_asset_namespace = os.getenv('SA_ONTOLOGY', 'http://ontology.tno.nl/secure_assets#')
    shacl_namespace = os.getenv('SHACL_NS', 'http://www.w3.org/ns/shacl#')
    VALIDATOR_URL = os.getenv('VALIDATE_URL', 'http://localhost:3030/api/validator')
    SECURE_ASSETS = rdflib.Namespace(secure_asset_namespace)
    NODE_ID = "{}[id]/".format(BDB_NS)

    context = {
        "sa": secure_asset_namespace,
        "xsd": "http://www.w3.org/2001/XMLSchema#",
        "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
        "rdfs": "http://www.w3.org/2000/01/rdf-schema#"
    }

    url_pattern = r"^http(s)?\:\/\/.+$"
    address_pattern = r"^[0-9A-Za-z]{43,44}$"
    datetime_pattern = r"^(?P<date>2\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))(T(?P<time>(([01]\d)|2[0-3]):([0-5]\d):([0-5]\d)))?$"
    n3_prefix_pattern = r"^@prefix\s(?P<prefix>\w+):\s<(?P<url>.+)>\s\.$"
    placeholder_pattern = r"{}\[id\]".format(BDB_NS)

    @staticmethod
    def return_uri_ref(uri):
        return rdflib.URIRef(uri)

    def load_rdf(self, serialized_data, data_format='json-ld'):
        g = rdflib.ConjunctiveGraph()

        try:
            g.parse(data=serialized_data, format=data_format)
        except:
            return False

        return g

    def generate_contexts(self, g):
        serialized = g.serialize(format='n3').decode()
        matches = re.finditer(self.n3_prefix_pattern, serialized, re.MULTILINE)
        context = {}
        for match in matches:
            context[match.group("prefix")] = match.group("url")
        return context

    def verify_rdf(self, serialized_data, data_format='json-ld', replace_id=None, filter_node=None):
        # replace ID placeholder
        if replace_id:
            serialized_data = re.sub(
                pattern=self.placeholder_pattern,
                repl="{}{}".format(BDB_NS, replace_id),
                string=serialized_data
            )

        g = self.load_rdf(serialized_data, data_format)
        if not g:
            return False

        # filter specific node
        if filter_node:
            filter_g = rdflib.Graph()
            node = rdflib.URIRef(filter_node)
            # TODO: implement

        # add contexts
        context = self.generate_contexts(g)

        # return serialized RDF
        return g.serialize(format='json-ld', context=context).decode()

    def get_nodes(self, data):
        g = self.load_rdf(json.dumps(data))

        nodes = []
        for n in g.all_nodes():
            if isinstance(n, rdflib.term.URIRef):
                if re.match(self.placeholder_pattern, n):
                    nodes.append(n)
        return nodes

    def get_matching_triples(self, data, subject=None, predicate=None, object_=None, as_string=True):
        g = self.load_rdf(json.dumps(data))

        triples = []
        for s, p, o in g.triples((subject, predicate, object_)):
            if as_string:
                triples.append((str(s), str(p), str(o)))
            else:
                triples.append((s, p, o))
        return triples

    def create_pgp(self, public_key, pgp_public):
        if not re.match(self.address_pattern, public_key):
            raise AttributeError('Please provide correct BigchainDB public key')

        # create pub-key asset
        g = rdflib.Graph()

        pgp_token = rdflib.URIRef(self.NODE_ID)
        g.add((pgp_token, RDF.type, self.SECURE_ASSETS.PGP_Public))
        g.add((pgp_token, self.SECURE_ASSETS.public_key, rdflib.Literal(pgp_public)))

        rdf_str = g.serialize(format='json-ld', context=self.context)

        return json.loads(rdf_str)

    def create_secure_msg_unlocker(self, crypto_keypair, symmetric_key, secure_asset_id, signatures):
        g = rdflib.Graph()

        unlocker = rdflib.URIRef(self.NODE_ID)
        g.add((unlocker, RDF.type, self.SECURE_ASSETS.Secure_Asset_Unlocker))
        g.add((unlocker, self.SECURE_ASSETS.public_key, rdflib.Literal(crypto_keypair.public_key)))
        g.add((unlocker, self.SECURE_ASSETS.private_key, rdflib.Literal(crypto_keypair.private_key)))
        g.add((unlocker, self.SECURE_ASSETS.symmetric_key, rdflib.Literal(symmetric_key)))
        g.add((unlocker, self.SECURE_ASSETS.secure_asset, rdflib.Literal(secure_asset_id)))

        for s in signatures:
            signature = rdflib.URIRef("{}{}".format(self.NODE_ID, s['sender']))
            g.add((signature, RDF.type, self.SECURE_ASSETS.Secure_Asset_Signature))
            g.add((signature, self.SECURE_ASSETS.public_key, rdflib.Literal(s['sender'])))
            g.add((signature, self.SECURE_ASSETS.signature_string, rdflib.Literal(s['signature'])))

            g.add((unlocker, self.SECURE_ASSETS.signature, signature))

        rdf_str = g.serialize(format='json-ld', context=self.context)

        return json.loads(rdf_str)

    def create_anonymous_sender_mapping(self, mapping):
        g = rdflib.Graph()

        mapping_node = rdflib.URIRef(self.NODE_ID)
        g.add((mapping_node, RDF.type, self.SECURE_ASSETS.Secure_Sender_Mapping))
        g.add((mapping_node, self.SECURE_ASSETS.sender, rdflib.Literal(mapping['sender'].public_key)))
        g.add((mapping_node, self.SECURE_ASSETS.anonymous_payload_public, rdflib.Literal(mapping['anonymous_payload_sender'].public_key)))
        g.add((mapping_node, self.SECURE_ASSETS.anonymous_payload_private, rdflib.Literal(mapping['anonymous_payload_sender'].private_key)))
        g.add((mapping_node, self.SECURE_ASSETS.anonymous_unlocker_public,
               rdflib.Literal(mapping['anonymous_unlocker_sender'].public_key)))
        g.add((mapping_node, self.SECURE_ASSETS.anonymous_unlocker_private, rdflib.Literal(mapping['anonymous_unlocker_sender'].private_key)))

        rdf_str = g.serialize(format='json-ld', context=self.context)

        return json.loads(rdf_str)

    def validate_rdf(self, rdf, shape):
        postdata = {
            'data': json.dumps(rdf),
            'data_type': 'application/ld+json',
            'shape': json.dumps(shape),
            'shape_type': 'application/ld+json'
        }

        r = requests.post(self.VALIDATOR_URL, data=postdata)
        if r.status_code == status.HTTP_200_OK:
            return r.json()
        return False

    def get_unlocker(self, data):
        secure_asset_id = self.get_matching_triples(
            data,
            subject=self.return_uri_ref(SecureSemantics.NODE_ID),
            predicate=self.SECURE_ASSETS.secure_asset
        )[0][2]

        secure_asset_cryptoid = namedtuple("CryptoId", ('public_key', 'private_key'))(
            self.get_matching_triples(
                data,
                subject=self.return_uri_ref(SecureSemantics.NODE_ID),
                predicate=self.SECURE_ASSETS.public_key
            )[0][2],
            self.get_matching_triples(
                data,
                subject=self.return_uri_ref(SecureSemantics.NODE_ID),
                predicate=SecureSemantics.SECURE_ASSETS.private_key
            )[0][2],
        )

        secure_asset_symmetric_key = self.get_matching_triples(
            data,
            subject=self.return_uri_ref(SecureSemantics.NODE_ID),
            predicate=self.SECURE_ASSETS.symmetric_key
        )[0][2]

        # get all signatures
        secure_asset_signatures = self.get_matching_triples(
            data,
            subject=self.return_uri_ref(SecureSemantics.NODE_ID),
            predicate=self.SECURE_ASSETS.signature,
            as_string=False
        )

        # loop over signatures
        for signature in secure_asset_signatures:
            sig = self.get_matching_triples(
                data,
                subject=signature[2],
                predicate=self.SECURE_ASSETS.signature_string
            )

            signer = self.get_matching_triples(
                data,
                subject=signature[2],
                predicate=self.SECURE_ASSETS.public_key
            )

            # Verify signature
            if not Crypto.verify_ed25519(sig[0][2], secure_asset_id.encode(), signer[0][2]):
                raise ValueError("Invalid signature")

        return namedtuple("Unlocker", ('asset_id', 'crypto_id', 'symmetric_key'))(
            secure_asset_id,
            secure_asset_cryptoid,
            secure_asset_symmetric_key
        )
