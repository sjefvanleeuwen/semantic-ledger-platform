# Copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research
import os
import json
from django.core.management.base import BaseCommand, CommandError
from api.models import CoreAssets
from api.serializers.serializers import CoreAssetSerializer
from api.bcdb.bdb import Bdb
from api.semantics.secure_semantics import SecureSemantics


class Command(BaseCommand):
    def handle(self, *args, **options):
        CoreAssets.objects.all().delete()

        bdb_URL = os.getenv('BDB_URL', '')
        self.stdout.write(bdb_URL)
        bdb = Bdb()
        secure_semantics = SecureSemantics()

        keypair = Bdb.generate_keypair()
        self.stdout.write("Keypair: {} - {}".format(keypair.public_key, keypair.private_key))

        shapes_dir = os.path.join(os.path.split(os.path.split(os.path.dirname(__file__))[0])[0], 'shapes')

        shapes = [
            {
                'file': 'PGP_Shape.ttl',
                'alias': 'PGP_shacl'
            },
            {
                'file': 'Unlocker_Shape.ttl',
                'alias': 'Unlocker_shacl'
            },
            {
                'file': 'Mapping_Shape.ttl',
                'alias': 'Mapping_shacl'
            },
        ]

        for shape in shapes:
            target_dir = os.path.join(shapes_dir, shape['file'])

            with open(target_dir) as f:
                shape_content = f.read()
                f.close()

                # Transform to JSON-LD
                publication_jsonld = secure_semantics.verify_rdf(shape_content, 'n3')

                # serialize payload
                payload = {
                    'rdf': json.loads(publication_jsonld)
                }

                # Specify recipients
                recipients = [
                    {'recipients': (keypair.public_key,),
                     'amount': 1},
                ]

                # TX
                tx = bdb.create_plain(
                    payload=payload,
                    senders=(keypair,),
                    recipients=recipients
                )

                # self.stdout.write("{}: {}".format(shape['alias'], tx['id']))

                core_asset = {
                    'alias': shape['alias'],
                    'asset_id': tx['id']
                }
                core_serializer = CoreAssetSerializer(data=core_asset)

                if core_serializer.is_valid():
                    try:
                        core_serializer.save()
                    except:
                        self.stderr.write("Could not save asset alias: %s" % shape['alias'])
                    self.stdout.write("%s saved (%s)" % (shape['alias'], tx['id']))
                else:
                    self.stderr.write("Invalid core asset: %s" % core_serializer.errors)
