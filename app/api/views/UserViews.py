# Copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.permissions import IsAdminUser
from rest_framework.permissions import AllowAny
from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.http import Http404

from django.db import DatabaseError
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404

from api.serializers.serializers import UserSerializer
from api.schemas import UserListSchema
from api.permissions import IsAdminOrReadOnly, IsAdminOrTargetUser


class UserListView(APIView):
    """
    List all, or create a new user
    """

    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication)
    permission_classes = (IsAdminUser,)

    # schema = UserListSchema()

    def get(selfs, requset):
        users = User.objects.all()
        serializer = UserSerializer(users, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = UserSerializer(data=request.data)
        if not serializer.is_valid():
            return Response("Invalid request", status=status.HTTP_400_BAD_REQUEST)

        serializer.save()

        return Response(serializer.data, status=status.HTTP_200_OK)


class UserDetailView(APIView):
    """
    Get, update or delete single user
    """
    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication)
    permission_classes = (IsAdminUser,)

    # schema = UserListSchema()

    def get_user(self, username):
        try:
            return User.objects.get(username=username)
        except User.DoesNotExist:
            raise Http404

    def get(self, request, username):
        user = self.get_user(username)
        serializer = UserSerializer(user)
        return Response(serializer.data)

    def delete(self, request, username):
        user = self.get_user(username)
        if user == request.user:
            return Response("You cannot delete yourself", status=status.HTTP_400_BAD_REQUEST)
        try:
            user.delete()
        except:
            return Response("Could not delete user", status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return Response(True)

    def put(self, request, username):
        user = self.get_user(username)
        serializer = UserSerializer(user, data=request.data, partial=True)
        if not serializer.is_valid():
            return Response("Invalid request %s" % serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        serializer.save()
        return Response("Updated %s" % username, status=status.HTTP_200_OK)
