# Copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated, IsAdminUser

from api.bcdb.bdb import Bdb


class LedgerStatus(APIView):
    """
    Return current status of BigchainDB ledger
    """
    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated, IsAdminUser)

    bdb = Bdb()

    def get(self, request):
        status = self.bdb.get_ledger_status()
        return Response(status)

