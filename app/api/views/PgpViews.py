# Copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research
import base64
from django.core.exceptions import ObjectDoesNotExist
from collections import namedtuple

from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from api.bcdb.bdb import Bdb, BDB_URL_FORMAT
from api.crypto.crypto import Crypto
from api.models import CryptoId, PgpKey, CoreAssets
from api.semantics.secure_semantics import SecureSemantics
from api.serializers.serializers import PgpInputSerializer, PgpSerializer
from api.schemas import PGPSchema


class AddPgpAsset2(APIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)

    # TODO: Schema
    # schema =

    def post(self, request):
        serializer = PgpSerializer(data=request.data)

        # validate input
        if not serializer.is_valid():
            return Response("Invalid request", status=status.HTTP_400_BAD_REQUEST)

        # Save PGP key
        pgp_key = serializer.save()

        # Create RDF
        publication_creator = SecureSemantics()
        try:
            rdf = publication_creator.create_pgp(pgp_key.crypto_id.public_key, pgp_key.public_key)
        except Exception as e:
            return Response('Could not save RDF: ' + str(e), status=status.HTTP_400_BAD_REQUEST)

        # TODO: PUBLISH TO LEDGER

        return Response(serializer.data, status=status.HTTP_200_OK)


class AddPgpAsset(APIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)

    # schema = PGPSchema()

    def post(self, request):
        # Validate input
        serializer = PgpInputSerializer(data=request.data)
        if not serializer.is_valid():
            return Response('Please specify correct input', status=status.HTTP_400_BAD_REQUEST)

        try:
            crypto_id = CryptoId.objects.get(user=request.user, alias=serializer.validated_data['crypto_id'])
        except ObjectDoesNotExist:
            return Response('Could not find Crypto-ID alias', status=status.HTTP_404_NOT_FOUND)

        try:
            PGP_shacl = CoreAssets.objects.get(alias='PGP_shacl')
        except ObjectDoesNotExist:
            return Response('Could not find PGP Shape asset reference', status=status.HTTP_404_NOT_FOUND)

        # Generate or use provided GPG key
        if not('pgp_public' in serializer.validated_data and
               'pgp_private' in serializer.validated_data and
               'passphrase' in serializer.validated_data):
            pgp = Crypto.gpg_create_key(
                crypto_id.public_key,
                "{}@{}".format(crypto_id.public_key, "bdb"),
                serializer.validated_data['private_key']
            )
        else:
            pubkey_bstr = serializer.validated_data['pgp_public'].encode()
            privkey_bstr = serializer.validated_data['pgp_private'].encode()
            pgp = {
                'public': base64.b64encode(pubkey_bstr).decode(),
                'private': base64.b64encode(privkey_bstr).decode(),
                'passphrase': serializer.validated_data['passphrase']
            }

        # create PGP RDF
        publication_creator = SecureSemantics()
        try:
            rdf = publication_creator.create_pgp(crypto_id.public_key, pgp['public'])
            pgp_key = PgpKey(
                crypto_id=crypto_id,
                public_b64=pgp['public'],
                private_b64=pgp['private'],
                passphrase=pgp['passphrase']
            )
            try:
                pgp_key.save()
            except:
                return Response('Unable to save PGP key', status=status.HTTP_400_BAD_REQUEST)

        except AttributeError as e:
            return Response('An error occured: ' + str(e), status=status.HTTP_400_BAD_REQUEST)

        # Compose publication
        bigchain_tx = {
            'rdf': rdf,
            'constraints': [BDB_URL_FORMAT.format(PGP_shacl.asset_id)]
        }

        bdb = Bdb()
        secure_semantics = SecureSemantics()
        shape_asset = bdb.get_asset(PGP_shacl.asset_id)
        if not shape_asset:
            return Response('Could not retrieve shape', status=status.HTTP_404_NOT_FOUND)

        rdf_valid = secure_semantics.validate_rdf(rdf, shape_asset['rdf'])
        if not rdf_valid['conforms']:
            return Response('PGP shape not valid', status=status.HTTP_400_BAD_REQUEST)

        C_ID = namedtuple('C_ID', ['public_key', 'private_key'])
        crypto_keypair = C_ID(crypto_id.public_key, serializer.validated_data['private_key'])

        # publish public key on-chain
        tx =bdb.create(
            payload=bigchain_tx,
            senders=(crypto_keypair,),
            recipients=[
                {'recipients': (crypto_id.public_key,)}
            ]
        )

        return Response({
            'crypto_id': crypto_id.public_key,
            'tx_id': tx['id']
        }, status=status.HTTP_200_OK)
