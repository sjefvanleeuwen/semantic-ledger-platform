# Copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research
from django.core.exceptions import ObjectDoesNotExist

from api.models import CryptoId
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from . import viewUtils as viewUtils

from api.bcdb.bdb import Bdb


class TransactionViews(APIView):
    def get_tx(self, tx_id):
        bdb = Bdb()
        tx = bdb.get_tx(tx_id)

        return tx


class FullTransactionView(TransactionViews):
    def get(self, request, tx_id):
        """
        Retrieve a transaction that is identified by @tx_id.

        Query parameters:
        ?block={True|False}: Also return the block height
        of the transaction.
        """
        try:
            tx = super().get_tx(tx_id)
        except AttributeError as e:
            return Response(str(e), status=status.HTTP_404_NOT_FOUND)
        
        # TODO this is incorrect as it will cast all non-empty
        # strings to True.
        # The proper way is through a serializer:
        # https://stackoverflow.com/questions/54983239/pass-url-parameter-to-serializer
        get_block = bool(request.GET.get('block', False))

        if get_block:
            #Try to retrieve the block height for the tx
            bdb = Bdb()
            try:
                block_height = bdb.get_block_height(tx_id)
            except Exception as e:
                # Something went wrong while retrieving block height;
                # Return a bad status
                return Response('Unable to retrieve block height: ' + str(e), status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            
            # Return a dict with the tx and the block height
            txdict = {'transaction':tx, 'block_height':block_height}
            return Response(txdict, status=status.HTTP_200_OK)

        # TODO Standardize return dict;
        # even without ?block it should just
        # give back {'transaction':tx}
        return Response(tx, status=status.HTTP_200_OK)


class TransactionInputsView(TransactionViews):
    def get(self, request, tx_id):
        try:
            tx = super().get_tx(tx_id)
        except AttributeError as e:
            return Response(str(e), status=status.HTTP_404_NOT_FOUND)

        return Response(tx['inputs'], status=status.HTTP_200_OK)


class TransactionOutputsView(TransactionViews):
    def get(self, request, tx_id):
        try:
            tx = super().get_tx(tx_id)
        except AttributeError as e:
            return Response(str(e), status=status.HTTP_404_NOT_FOUND)

        return Response(tx['outputs'], status=status.HTTP_200_OK)

class TransactionHistoryView(APIView):
    def get(self, request, crypto_id):
        
        # Parse query arguments
        created = viewUtils.parse_bool_query_arg(request, 'created')

        # get crypto-id
        try:
            pubkey = CryptoId.objects.get(user=request.user, alias=crypto_id).public_key
        except ObjectDoesNotExist:
            return Response('Crypto ID not found', status=status.HTTP_404_NOT_FOUND)

        # Retrieve all transactions
        # Instantiate bdb
        bdb = Bdb()
        # Get all transactions that gave the user ownership of an asset,
        # or that contain assets the user created.
        try:
            transactions = bdb.get_transactions_of_user(public_key=pubkey, spent=None, created=created)
        except Exception as e:
            # TODO catch specific exceptions
            return Response(str(e), status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        
        return Response(transactions, status=status.HTTP_200_OK)

class FromAssetView(APIView):
    def get(self, request, asset_id):
        """
        Retrieve all transactions related to an asset.
        """
        # Parse query args
        sort = viewUtils.parse_bool_query_arg(request, 'sort')

        bdb = Bdb()

        try:
            txs = bdb.get_transactions_of_asset(asset_id, sort=sort)
            return Response(txs, status=status.HTTP_200_OK)
        except Exception as e:
            # TODO catch specific exceptions
            return Response('Could not retrieve transactions: ' + str(e), status=status.HTTP_500_INTERNAL_SERVER_ERROR)
