# Copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research
import json
import re

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated

from django.core.exceptions import ObjectDoesNotExist

from api.bcdb.bdb import Bdb
from api.crypto.crypto import Crypto
from api.models import CryptoId, PgpKey
from api.schemas import GetPublicationSchema, GetRDFSchema, AssetBalanceSchema
from api.semantics.secure_semantics import SecureSemantics

from . import viewUtils as viewUtils

class AssetViewBase(APIView):
    def get_semantic_asset(self, request, asset_id):
        bdb = Bdb()
        asset = bdb.get_asset(asset_id)
        if asset:
            if 'rdf' in asset:
                # Plain data, return asset
                return asset
            elif 'secure_payload_unlocker' in asset:
                # Secure data, get secure data and unlock this
                pgp_keys = PgpKey.objects.filter(crypto_id__in=CryptoId.objects.filter(user=request.user))

                for pgp_key in pgp_keys:
                    decrypted_asset = Crypto.gpg_decrypt(
                        pgp_key.private_b64,
                        pgp_key.passphrase,
                        asset['secure_payload_unlocker']
                    )
                    if decrypted_asset:
                        decrypted_asset = json.loads(decrypted_asset)
                        secure_semantics = SecureSemantics()
                        if secure_semantics.verify_rdf(json.dumps(decrypted_asset['rdf'])):
                            # Get locked data
                            secure_asset_id = secure_semantics.get_matching_triples(
                                decrypted_asset['rdf'],
                                subject=secure_semantics.return_uri_ref(SecureSemantics.NODE_ID),
                                predicate=SecureSemantics.SECURE_ASSETS.secure_asset
                            )[0][2]
                            secure_asset = bdb.get_asset(secure_asset_id)
                            if secure_asset:
                                # unlock locked data
                                crypto = Crypto()
                                token = secure_semantics.get_matching_triples(
                                    decrypted_asset['rdf'],
                                    subject=secure_semantics.return_uri_ref(SecureSemantics.NODE_ID),
                                    predicate=SecureSemantics.SECURE_ASSETS.symmetric_key
                                )[0][2]
                                if token:
                                    decrypted_payload = crypto.fernet_decrypt(token, secure_asset['secure_payload'])
                                    if decrypted_payload:
                                        decrypted_payload = json.loads(decrypted_payload)
                                        return decrypted_payload
                return False
            elif 'secure_mapping' in asset:
                # Secure data, get secure data and unlock this
                pgp_keys = PgpKey.objects.filter(crypto_id__in=CryptoId.objects.filter(user=request.user))

                for pgp_key in pgp_keys:
                    decrypted_asset = Crypto.gpg_decrypt(
                        pgp_key.private_b64,
                        pgp_key.passphrase,
                        asset['secure_mapping']
                    )
                    if decrypted_asset:
                        decrypted_asset = json.loads(decrypted_asset)
                        return decrypted_asset
                return False
        else:
            return False


class GetAssetView(APIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)

    # schema = GetPublicationSchema()

    def get(self, request, crypto_id):
        # Check query parameters
        # @asData: return the asset data, rather than the IDs.
        asData = viewUtils.parse_bool_query_arg(request, key='asData')

        # get crypto-id
        try:
            _id = CryptoId.objects.get(user=request.user, alias=crypto_id)
        except ObjectDoesNotExist:
            return Response('Crypto ID not found', status=status.HTTP_404_NOT_FOUND)

        bdb = Bdb()
        # Decide getter function based on @asData.
        if asData:
            try:
                returnValue = bdb.get_assets(_id.public_key)
            except RuntimeError as e:
                return(Response("Could not list assets: {}".format(str(e)),
                    status=status.HTTP_500_INTERNAL_SERVER_ERROR))
        else:
            returnValue = bdb.get_asset_ids(_id.public_key)
        # Check if a valid return came back
        if returnValue:
            return Response(returnValue, status=status.HTTP_200_OK)
        else:
            return Response([], status=status.HTTP_200_OK)


class AssetView(AssetViewBase):
    def get(self, request, asset_id):
        bdb = Bdb()
        asset = bdb.get_asset(asset_id)
        if not asset:
            return Response("Asset not found", status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(asset, status=status.HTTP_200_OK)


class SemanticAssetView(AssetViewBase):
    def get(self, request, asset_id):
        asset = self.get_semantic_asset(asset_id)
        if not asset:
            return Response("Asset not found", status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(asset, status=status.HTTP_200_OK)


class RdfAssetView(AssetViewBase):
    # schema = GetRDFSchema()

    def get(self, request, asset_id, rdf_node=None):
        asset = self.get_semantic_asset(request, asset_id)
        if not asset:
            return Response("Asset not found", status=status.HTTP_404_NOT_FOUND)
        if asset and 'rdf' in asset:
            secure_semantics = SecureSemantics()
            # Verify data is RDF (json-ld)
            verified_rdf = secure_semantics.verify_rdf(
                json.dumps(asset['rdf']),
                replace_id=asset_id,
                filter_node=rdf_node
            )
            if not verified_rdf:
                return Response("Asset is not JSON-LD", status=status.HTTP_404_NOT_FOUND)
            return Response(json.loads(verified_rdf), status=status.HTTP_200_OK)
        return Response("Asset not found", status=status.HTTP_404_NOT_FOUND)


class ValidatorViews(AssetViewBase):
    """
    Validate if asset-id conforms to shape associated with asset
    """
    def get(self, request, asset_id):
        asset = self.get_semantic_asset(request, asset_id)
        # return 404 if asset is not found
        if not asset or not('rdf' in asset.keys() and 'constraints' in asset.keys()):
            return Response("Asset not found or it's not a semantic asset", status=status.HTTP_404_NOT_FOUND)

        bdb = Bdb()
        secure_semantics = SecureSemantics()

        rdf_str = secure_semantics.verify_rdf(json.dumps(asset['rdf']))
        if not rdf_str:
            return Response('Invalid RDF', status=status.HTTP_400_BAD_REQUEST)
        rdf = json.loads(rdf_str)

        if not len(asset['constraints']):
            return Response('No constraints found', status=status.HTTP_404_NOT_FOUND)

        reports = []
        for constraint in asset['constraints']:
            # get shape, either directly from this asset or referenced asset
            url_match = re.match(bdb.bdb_asset_pattern, constraint) if type(constraint) is str else False
            if url_match:
                shape_asset = bdb.get_asset(url_match.group('asset_id'))
                shape = shape_asset['rdf']
            else:
                shape_str = secure_semantics.verify_rdf(json.dumps(constraint))
                if shape_str:
                    shape = json.loads(shape_str)
                else:
                    return Response('Invalid shape', status=status.HTTP_400_BAD_REQUEST)
            validation = secure_semantics.validate_rdf(rdf, shape)
            if not validation:
                return Response('Unable to validate RDF', status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            reports.append(validation)
        return Response(reports, status=status.HTTP_200_OK)


class AssetUnspentView(AssetViewBase):
    # schema = AssetBalanceSchema()

    def get(self, request, asset_id, crypto_id=None):
        # Get unspent outputs of asset
        bdb = Bdb()
        outputs = bdb.get_asset_uspent_outputs(asset_id)

        if not crypto_id:
            return Response(outputs, status=status.HTTP_200_OK)
        else:
            relevant = []
            for output in outputs:
                if crypto_id in output['public_keys']:
                    relevant.append(output)
            return Response(relevant, status=status.HTTP_200_OK)