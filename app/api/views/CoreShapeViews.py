# Copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.http import Http404

from api.serializers.serializers import CoreAssetSerializer
from api.models import CoreAssets
from api.schemas import CoreAssetsSchema
from api.permissions import IsAdminOrReadOnly


class CoreShapeListViews(APIView):
    """
    Set or return platform core shapes
    """

    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication)
    permission_classes = (IsAdminOrReadOnly,)

    # schema = CoreAssetsSchema()

    def get(self, request):
        core_assets = CoreAssets.objects.all()
        serializer = CoreAssetSerializer(core_assets, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request):
        serializer = CoreAssetSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.validated_data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CoreShapeDetailView(APIView):
    """
    Set or return platform core shapes
    """

    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication)
    permission_classes = (IsAdminOrReadOnly,)

    def get_object(self, alias):
        try:
            return CoreAssets.objects.get(alias=alias)
        except CoreAssets.DoesNotExist:
            raise Http404

    def get(self, request, alias):
        coreshape = self.get_object(alias)
        serializer = CoreAssetSerializer(coreshape)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def delete(self, request, alias):
        coreshape = self.get_object(alias)
        try:
            coreshape.delete()
        except:
            return Response("Could not delete core-shape", status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return Response(True)

    def put(self, request, alias):
        coreshape = self.get_object(alias)
        serializer = CoreAssetSerializer(coreshape, data=request.data)
        if not serializer.is_valid():
            return Response("Invalid request", status=status.HTTP_400_BAD_REQUEST)
        serializer.save()
        return Response("Updated %s" % alias, status=status.HTTP_200_OK)
