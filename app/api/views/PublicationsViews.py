# Copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research
import json
import re
from django.core.exceptions import ObjectDoesNotExist
from collections import namedtuple

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated

from api.bcdb.bdb import Bdb, BDB_URL_FORMAT
from api.models import CryptoId, PgpKey, AssetReference, CoreAssets
from api.semantics.secure_semantics import SecureSemantics
from api.crypto.crypto import Crypto
from api.serializers.serializers import \
    AssetAliasSerializer, PublicationInputSerializer, TransferInputSerializer, RetrieveSecureAssetSerializer
from api.schemas import PublicationSchema, AssetAliasSchema, \
    SecurePublicationSchema, TransferAssetSchema


class PublicationsView(APIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)

    # schema = PublicationSchema()

    def post(self, request):
        bdb = Bdb()
        serializer = PublicationInputSerializer(data=request.data)

        if not serializer.is_valid():
            return Response('Please provide correct publication', status=status.HTTP_400_BAD_REQUEST)

        try:
            crypto_id = CryptoId.objects.get(user=request.user, alias=serializer.validated_data['crypto_id'])
        except ObjectDoesNotExist:
            return Response('Could not find Crypto-ID alias', status=status.HTTP_404_NOT_FOUND)

        secure = serializer.validated_data['secure']

        secure_publish_shape_assets = None
        if secure:
            try:
                pgp_shape_reference = CoreAssets.objects.get(alias='PGP_shacl')
                unlocker_shape_reference = CoreAssets.objects.get(alias='Unlocker_shacl')
                mapping_shape_reference = CoreAssets.objects.get(alias='Mapping_shacl')
            except ObjectDoesNotExist:
                return Response('Could not find all required shape asset references', status=status.HTTP_404_NOT_FOUND)

            secure_publish_shape_assets = {
                'pgp_shape_id': BDB_URL_FORMAT.format(
                    pgp_shape_reference.asset_id
                ),
                'unlocker_shape_id': BDB_URL_FORMAT.format(
                    unlocker_shape_reference.asset_id
                ),
                'mapping_shape_id': BDB_URL_FORMAT.format(
                    mapping_shape_reference.asset_id
                )
            }

        # Verify RDF data
        dataformat = serializer.validated_data['format']

        secure_semantics = SecureSemantics()
        publication_jsonld = secure_semantics.verify_rdf(serializer.validated_data['publication'], dataformat)

        if not publication_jsonld:
            return Response('Invalid publication', status=status.HTTP_400_BAD_REQUEST)

        # BigchainDB publication
        payload = {
            'rdf': json.loads(publication_jsonld),
            'constraints': []
        }

        # add shape, if provided
        if 'shapes' in serializer.validated_data:
            for shape_input in serializer.validated_data['shapes']:
                uri_match = re.match(r'^bdb:\/\/(?P<asset_id>[a-f0-9]+)\/$', shape_input['shape'])
                if uri_match:
                    asset_id = uri_match.group('asset_id')
                    asset = bdb.get_asset(asset_id)
                    shape = secure_semantics.verify_rdf(
                        json.dumps(asset['rdf']),
                        data_format='json-ld'
                    )
                else:
                    shape = secure_semantics.verify_rdf(
                        shape_input['shape'],
                        data_format=shape_input['shape_format']
                    )

                if shape:
                    shape_object = json.loads(shape)
                    rdf_validation = secure_semantics.validate_rdf(payload['rdf'], shape_object)
                    if not rdf_validation['conforms']:
                        return Response(rdf_validation, status=status.HTTP_400_BAD_REQUEST)

                    payload['constraints'].append(
                        shape_input['shape'] if uri_match else json.loads(shape)
                    )
                else:
                    return Response('Invalid shape', status=status.HTTP_400_BAD_REQUEST)

        if 'recipients' in serializer.validated_data:
            recipients = serializer.validated_data['recipients']
        else:
            # if recipients is not specified: Assign to self
            # amount to-be-created
            amount = serializer.validated_data['amount']
            recipients = [
                {'recipients': (crypto_id.public_key,),
                 'amount': amount}
            ]

        C_ID = namedtuple('C_ID', ['public_key', 'private_key'])
        crypto_id_keypair = C_ID(crypto_id.public_key, serializer.validated_data['private_key'])

        try:
            tx = bdb.create(
                payload=payload,
                senders=(crypto_id_keypair,),
                recipients=recipients,
                secure=secure,
                secure_publish_shape_assets=secure_publish_shape_assets
            )
        except ValueError as e:
            return Response("Could not send transaction: {}".format(e), status=status.HTTP_400_BAD_REQUEST)

        # Save asset with 'alias'
        if 'asset_alias' in serializer.validated_data:
            asset_reference = AssetReference()
            asset_reference.alias = serializer.validated_data['asset_alias']
            asset_reference.asset_id = tx['id']
            asset_reference.crypto_id = crypto_id
            asset_reference.save()

        return Response(tx['id'], status=status.HTTP_200_OK)


class AssetAliasView(APIView):
    """
    get:
    Return all asset-aliasses

    post:
    Store references to asset-ids
    """
    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)

    # schema = AssetAliasSchema()

    def post(self, request, alias=None):
        serializer = AssetAliasSerializer(data=request.data)
        if not serializer.is_valid():
            return Response('Please provide alias and publication', status=status.HTTP_400_BAD_REQUEST)

        try:
            crypto_id = CryptoId.objects.get(user=request.user, alias=serializer.validated_data['crypto_id'])
        except ObjectDoesNotExist:
            return Response('Could not find Crypto-ID alias', status=status.HTTP_404_NOT_FOUND)

        asset_reference = AssetReference()
        asset_reference.alias = serializer.validated_data['alias']
        asset_reference.asset_id = serializer.validated_data['asset_id']
        asset_reference.crypto_id = crypto_id

        try:
            asset_reference.save()
        except:
            return Response('Unable to save asset-alias', status=status.HTTP_400_BAD_REQUEST)

        return Response(True, status=status.HTTP_200_OK)

    def get(self, request, alias=None):

        try:
            crypto_ids = CryptoId.objects.filter(user=request.user)
        except ObjectDoesNotExist:
            return Response('Could not find crypto-ids of current user', status=status.HTTP_404_NOT_FOUND)

        if alias is None:
            try:
                aliasses = AssetReference.objects.filter(crypto_id__in=crypto_ids)
            except ObjectDoesNotExist:
                return Response('Could not find aliasses of specified crypto-id', status=status.HTTP_404_NOT_FOUND)

            serializer = AssetAliasSerializer(aliasses, many=True)
        else:
            try:
                alias = AssetReference.objects.get(crypto_id__in=crypto_ids, alias=alias)
            except ObjectDoesNotExist:
                return Response('Could not find asset reference', status=status.HTTP_404_NOT_FOUND)

            serializer = AssetAliasSerializer(alias)
        return Response(serializer.data, status=status.HTTP_200_OK)


class SecureAssetView(APIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)

    # schema = SecurePublicationSchema()

    def post(self, request):
        serializer = RetrieveSecureAssetSerializer(data=request.data)
        if not serializer.is_valid():
            return Response('Please provide asset_id', status=status.HTTP_400_BAD_REQUEST)

        bdb = Bdb()

        # See if asset is indeed an unlocker
        asset = bdb.get_asset(serializer.validated_data['asset_id'])

        if not('secure_payload_unlocker' in asset.keys()):
            return Response('Invalid unlocker asset', status=status.HTTP_400_BAD_REQUEST)

        # get unspent output of this unlocker
        outputs = bdb.get_asset_uspent_outputs(serializer.validated_data['asset_id'])
        if len(outputs) != 1 or len(outputs[0]['public_keys']) != 1:
            return Response('Invalid unlocker, multiple owners', status=status.HTTP_400_BAD_REQUEST)

        # get crypto id of this unlocker
        try:
            crypto_id = CryptoId.objects.get(user=request.user, public_key=outputs[0]['public_keys'][0])
        except ObjectDoesNotExist:
            return Response('Could not find Crypto-ID alias', status=status.HTTP_404_NOT_FOUND)

        try:
            unlocker_shape_reference = CoreAssets.objects.get(alias='Unlocker_shacl')
        except ObjectDoesNotExist:
            return Response('Could not find unlocker core asset references', status=status.HTTP_404_NOT_FOUND)

        unlocker_shape = BDB_URL_FORMAT.format(
            unlocker_shape_reference.asset_id
        )

        # get PGP keys associated to this user
        pgp_keys = PgpKey.objects.filter(crypto_id=crypto_id)

        # decrypt ulocker
        decrypted_unlocker = None
        for pgp_key in pgp_keys:
            decrypted_unlocker = Crypto.gpg_decrypt(pgp_key.private_b64,
                                                    pgp_key.passphrase,
                                                    asset['secure_payload_unlocker'])

        # if successfully unlocked unlocker
        if not decrypted_unlocker:
            return Response('Unable read unlocker asset', status=status.HTTP_400_BAD_REQUEST)
        decrypted_unlocker = json.loads(decrypted_unlocker)

        secure_semantics = SecureSemantics()

        # valid RDF?
        if not secure_semantics.verify_rdf(json.dumps(decrypted_unlocker['rdf'])):
            return Response('Invalid RDF', status=status.HTTP_400_BAD_REQUEST)

        try:
            unlocker = secure_semantics.get_unlocker(decrypted_unlocker['rdf'])
        except ValueError as err:
            return Response(err, status=status.HTTP_400_BAD_REQUEST)

        C_ID = namedtuple('C_ID', ['public_key', 'private_key', 'secure_received_public'])
        crypto_id_keypair = C_ID(
            crypto_id.public_key,
            serializer.validated_data['private_key'],
            crypto_id.secure_received_public
        )

        # retrieve asset (transfer to own crypto_id)
        retrieve_tx = bdb.retrieve_secure_asset(
            unlocker.asset_id,
            unlocker.crypto_id,
            unlocker.symmetric_key,
            serializer.validated_data['asset_id'],
            crypto_id_keypair,
            unlocker_shape
        )
        return Response(retrieve_tx, status=status.HTTP_200_OK)


class TransferAssetView(APIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)

    # schema = TransferAssetSchema()

    def post(self, request):
        serializer = TransferInputSerializer(data=request.data)
        if not serializer.is_valid():
            return Response('Please provide correct arguments', status=status.HTTP_400_BAD_REQUEST)

        bdb = Bdb()

        # get crypto-id
        try:
            crypto_id = CryptoId.objects.get(user=request.user, alias=serializer.validated_data['crypto_id'])
        except ObjectDoesNotExist:
            return Response('Could not find Crypto-ID alias %s' % serializer.validated_data['crypto_id'], status=status.HTTP_404_NOT_FOUND)

        C_ID = namedtuple('C_ID', ['public_key', 'private_key', 'secure_received_public'])
        crypto_id_keypair = C_ID(
            crypto_id.public_key,
            serializer.validated_data['private_key'],
            crypto_id.secure_received_public
        )

        secure = serializer.validated_data['secure']

        if secure:
            try:
                pgp_shape_reference = CoreAssets.objects.get(alias='PGP_shacl')
            except ObjectDoesNotExist:
                return Response(
                    'Semantic Ledger Instance Error: Could not find required PGP shape asset references',
                    status=status.HTTP_404_NOT_FOUND
                )

            try:
                unlocker_shape_reference = CoreAssets.objects.get(alias='Unlocker_shacl')
            except ObjectDoesNotExist:
                return Response(
                    'Semantic Ledger Instance Error: Could not find required Unlocker shape asset references',
                    status=status.HTTP_404_NOT_FOUND
                )

            try:
                mapping_shape_reference = CoreAssets.objects.get(alias='Mapping_shacl')
            except ObjectDoesNotExist:
                return Response(
                    'Semantic Ledger Instance Error: Could not find required mapping shape asset references',
                    status=status.HTTP_404_NOT_FOUND
                )

            secure_publish_shape_assets = {
                'pgp_shape_id': BDB_URL_FORMAT.format(
                    pgp_shape_reference.asset_id
                ),
                'unlocker_shape_id': BDB_URL_FORMAT.format(
                    unlocker_shape_reference.asset_id
                ),
                'mapping_shape_id': BDB_URL_FORMAT.format(
                    mapping_shape_reference.asset_id
                )
            }

            try:
                pgps = PgpKey.objects.filter(crypto_id=crypto_id)
            except ObjectDoesNotExist:
                return Response('Could not find PGP keys for this crypto-id')

            transfer_tx = bdb.transfer_secure(
                unlocker_asset_id=serializer.validated_data['asset_id'],
                sender=crypto_id_keypair,
                sender_pgps=pgps,
                recipients=serializer.validated_data['recipients'],
                secure_publish_shape_assets=secure_publish_shape_assets
            )

        else:
            # Check if metadata was provided with the call
            metadata = None
            if 'metadata' in serializer.validated_data:
                metadata = serializer.validated_data['metadata']

            try:
                transfer_tx = bdb.transfer_plain(
                    asset_id=serializer.validated_data['asset_id'],
                    senders=(crypto_id_keypair,),
                    recipients=serializer.validated_data['recipients'],
                    metadata=metadata
                )
            except Exception as e:
                return Response("Error in transfer_plain: {}".format(str(e)), status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        if not transfer_tx:
            return Response("Something went wrong", status=status.HTTP_400_BAD_REQUEST)

        return Response(transfer_tx, status=status.HTTP_200_OK)
