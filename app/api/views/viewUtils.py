# Copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research
"""
Helper functions for view classes.

Author: Jeroen Breteler
Author affiliation: TNO
Date created: January 8, 2020
"""
# TODO Possibly these should become functions
# for an SLPView type object.

def parse_bool_query_arg(request, key, default_value=None):
    """
    Parse a query argument as a boolean value.

    @request is the request containing potential query arguments
    @key is the name of the query argument field
    """
    # Get the argument value, if any
    # Default to :default_value
    arg = request.GET.get(key, default_value)
    try:
        # If there was a string value,
        # decide on True or False
        return arg.lower() in ["true", "1"]
    except AttributeError:
        # We are not working with a string;
        # the argument is unacceptable
        return default_value
