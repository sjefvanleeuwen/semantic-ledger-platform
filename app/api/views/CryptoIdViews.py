# Copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.http import Http404

from api.models import CryptoId
from api.serializers.serializers import CryptoIdSerializer
from api.bcdb.bdb import Bdb
from api.schemas import CryptoIdSchema


class CryptoIdListView(APIView):
    """
    List all, or set a crypto-id
    """

    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)

    # schema = CryptoIdSchema()

    def get(self, request):
        settings = CryptoId.objects.filter(user=request.user)
        serializer = CryptoIdSerializer(settings, many=True)
        return Response(serializer.data)

    def post(self, request, **kwargs):
        # Keypairs
        keys = Bdb.generate_keypair()
        secure_received_id = Bdb.generate_keypair()

        input_object = {
            'user': request.user.pk,
            'alias': request.data['alias'],
            'public_key': keys.public_key,
            'secure_received_public': secure_received_id.public_key,
        }

        # Serializer
        serializer = CryptoIdSerializer(data=input_object)

        if not serializer.is_valid():
            return Response("Invalid request, %s" % serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        try:
            serializer.save()
        except Exception as e:
            return Response('Unable to save BigchainDB ID: %s' % e, status=status.HTTP_400_BAD_REQUEST)

        response_data = {
            'keypair': {
                'public_key': keys.public_key,
                'private_key': keys.private_key
            },
            'received': {
                'public_key': secure_received_id.public_key,
                'private_key': secure_received_id.private_key
            }
        }
        return Response(response_data, status=status.HTTP_200_OK)


class CryptoIdDetail(APIView):
    """
    Retrieve, delete or update specific Crypto-ID
    """
    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)

    # schema = SettingDetailSchema()

    def get_object(self, user, alias):
        try:
            return CryptoId.objects.get(user=user, alias=alias)
        except CryptoId.DoesNotExist:
            raise Http404

    def get(self, request, alias):
        _id = self.get_object(user=request.user, alias=alias)
        serializer = CryptoIdSerializer(_id)
        return Response(serializer.data)

    def delete(self, request, alias):
        _id = self.get_object(user=request.user, alias=alias)
        try:
            _id.delete()
        except:
            return Response("Could not delete Crypto ID", status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return Response(True)

    # TODO: PUT call
    def put(self, request, alias):
        _id = self.get_object(user=request.user, alias=alias)

        # Keypairs
        keys = Bdb.generate_keypair()
        secure_received_id = Bdb.generate_keypair()

        input_object = {
            'user': request.user.pk,
            'public_key': keys.public_key,
            'secure_received_public': secure_received_id.public_key,
        }

        # Serializer
        serializer = CryptoIdSerializer(_id, data=input_object, partial=True)

        if not serializer.is_valid():
            return Response("Invalid request, %s" % serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        try:
            serializer.save()
        except Exception as e:
            return Response('Unable to save BigchainDB ID: %s' % e, status=status.HTTP_400_BAD_REQUEST)

        response_data = {
            'keypair': {
                'public_key': keys.public_key,
                'private_key': keys.private_key
            },
            'received': {
                'public_key': secure_received_id.public_key,
                'private_key': secure_received_id.private_key
            }
        }
        return Response(response_data, status=status.HTTP_200_OK)

# Retrieve outputs per SLP ID
class OutputView(APIView):
    """Retrieve an SLP ID's unspent outputs."""
    
    def get(self, request, alias):
        user = request.user
        try:
            cryptoId = CryptoId.objects.get(user=user, alias=alias)
        except CryptoId.DoesNotExist:
            print("CryptoID retrieval failed: {}".format(str(e)))
            return Response(str(e), status=status.HTTP_400_BAD_REQUEST)

        try:
            bdb = Bdb()
            return Response(bdb.bdb.outputs.get(cryptoId.public_key, spent=False), status=status.HTTP_200_OK)
        except Exception as e:
            print("Bdb call failed: {}".format(str(e)))
            return Response(str(e), status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class OutputPubkeyView(APIView):
    """Retrieve an SLP ID's unspent outputs, identifying through public key."""

    def get(self, request, pubkey):
        try:
            bdb = Bdb()
            return Response(bdb.bdb.outputs.get(pubkey, spent=False), status=status.HTTP_200_OK)
        except Exception as e:
            print("Bdb call failed: {}".format(str(e)))
            return Response(str(e), status=status.HTTP_500_INTERNAL_SERVER_ERROR)