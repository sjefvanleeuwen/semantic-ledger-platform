# Copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research
from rest_framework.schemas import AutoSchema, ManualSchema
from rest_framework.compat import coreapi, coreschema


class CryptoIdSchema(AutoSchema):
    """
    Overrides `get_manual_fields()` to provide correct schema
    """

    def get_manual_fields(self, path, method):
        extra_fields = []
        if method == 'POST':
            extra_fields = [
                coreapi.Field(
                    name="alias",
                    required=True,
                    location='form',
                    schema=coreschema.String(
                        title="Crypto ID Alias",
                        description="Crypto ID Alias to store EdDSA ledger keypair (Crypto ID)"
                    )
                )
            ]

        manual_fields = super().get_manual_fields(path, method)
        return manual_fields + extra_fields


class PGPSchema(ManualSchema):
    def __init__(self):
        fields = [
            coreapi.Field(
                name="crypto_id",
                required=True,
                location='form',
                schema=coreschema.String(
                    title="Crypto ID",
                    description="To which Crypto ID should the PGP key be linked?"
                )
            ),
            coreapi.Field(
                name="pgp_public",
                required=False,
                location='form',
                schema=coreschema.String(
                    title='PGP public key',
                    description='Public part of PGP keypair'
                )
            ),
            coreapi.Field(
                name="pgp_private",
                required=False,
                location='form',
                schema=coreschema.String(
                    title='PGP private key',
                    description='Private part of PGP keypair'
                )
            ),
            coreapi.Field(
                name="passphrase",
                required=False,
                location='form',
                schema=coreschema.String(
                    title='PGP passphrase',
                    description='Passphrase for PGP key'
                )
            )
        ]

        description = "Register a new PGP asset on the ledger, PGP_shacl asset should already be registerred"

        super().__init__(fields, description)


class PublicationSchema(ManualSchema):
    def __init__(self):
        fields = [
            coreapi.Field(
                name="crypto_id",
                required=True,
                location='form',
                schema=coreschema.String(
                    title="Crypto ID",
                    description="Crypto ID to publish with"
                )
            ),
            coreapi.Field(
                name="private_key",
                required=True,
                location='form',
                schema=coreschema.String(
                    title="Private key",
                    description="Private key of crypto ID"
                )
            ),
            coreapi.Field(
                name="publication",
                required=True,
                location='form',
                schema=coreschema.String(
                    title="Publication",
                    description="Publication content"
                )
            ),
            coreapi.Field(
                name="format",
                required=False,
                location='form',
                schema=coreschema.String(
                    title="Publication format",
                    description="Publication format: json-ld or n3",
                    default='json-ld'
                )
            ),
            coreapi.Field(
                name="secure",
                required=False,
                location='form',
                schema=coreschema.Boolean(
                    title="Secure",
                    description="Secure or public publication",
                    default=False
                )
            ),
            # coreapi.Field(
            #     name="shape",
            #     required=False,
            #     location='form',
            #     schema=coreschema.String(
            #         title="Shape",
            #         description="Shape publication conforms to."
            #     )
            # ),
            # coreapi.Field(
            #     name="shape_format",
            #     required=False,
            #     location='form',
            #     schema=coreschema.String(
            #         title="Shape format",
            #         description="Shape format: json-ld or n3",
            #         default='n3'
            #     )
            # ),
            coreapi.Field(
                name="shapes",
                required=False,
                location='form',
                schema=coreschema.Array(
                    title="Shapes",
                    description="Shapes this publication follows",
                    items=coreschema.Object(
                        title="Shape",
                        description="Shape definition",
                        properties={
                            'shape': coreschema.String(title="Shape", description="Shape data"),
                            'shape_format': coreschema.String(
                                title='Shape format',
                                description='Shape format: json-ld or n3',
                                default='n3'
                            )
                        }
                    ),
                    min_items=1
                ),
            ),
            coreapi.Field(
                name="recipients",
                required=False,
                location='form',
                schema=coreschema.String(
                    title="Recipients",
                    description="Recipients of transaction"
                )
            ),
            coreapi.Field(
                name="amount",
                required=False,
                location='form',
                schema=coreschema.Integer(
                    title="Amount",
                    description="Amount of assets to create",
                    default=1
                )
            ),
            coreapi.Field(
                name="asset_alias",
                required=False,
                location='form',
                schema=coreschema.String(
                    title="Asset alias",
                    description="Local alias to store asset-id"
                )
            ),
        ]

        description = "Publish new data to the ledger"

        super().__init__(fields, description)


class GetPublicationSchema(ManualSchema):
    def __init__(self):
        fields = [
             coreapi.Field(
                 name="crypto_id",
                 required=True,
                 location='path',
                 schema=coreschema.String(
                     title="Crypto ID",
                     description="Crypto ID to return asset-ids from"
                 )
             )
         ]
        description = "Get asset-ids of Crypto ID"

        super().__init__(fields, description)


class AssetAliasSchema(AutoSchema):
    """
    Overrides `get_manual_fields()` to provide correct schema
    """
    def get_manual_fields(self, path, method):
        extra_fields = []
        if method == 'GET':
            extra_fields = [
                coreapi.Field(
                    name="alias",
                    required=False,
                    location='path',
                    schema=coreschema.String(
                        title="Asset alias",
                        description="Asset alias to retrieve"
                    )
                )
            ]
        if method == 'POST':
            extra_fields = [
                coreapi.Field(
                    name="crypto_id",
                    required=True,
                    location='form',
                    schema=coreschema.String(
                        title="Crypto ID",
                        description="To which crypto_id should the asset-id be linked?"
                    )
                ),
                coreapi.Field(
                    name="asset_id",
                    required=True,
                    location='form',
                    schema=coreschema.String(
                        title="Asset ID",
                        description="Asset-id to store"
                    )
                ),
                coreapi.Field(
                    name="alias",
                    required=True,
                    location='form',
                    schema=coreschema.String(
                        title="Asset alias",
                        description="Asset alias to store"
                    )
                )
            ]

        manual_fields = super().get_manual_fields(path, method)
        return manual_fields + extra_fields


class GetRDFSchema(ManualSchema):
    def __init__(self):
        fields = [
            coreapi.Field(
                name="asset_id",
                required=True,
                location='path',
                schema=coreschema.String(
                    title="Asset ID",
                    description="Asset-id to retrieve"
                )
            ),
            coreapi.Field(
                name="rdf_node",
                required=False,
                location='path',
                schema=coreschema.String(
                    title="RDF Node",
                    description="RDF node to retrieve from document"
                )
            )
        ]
        description="Return RDF resource"
        super().__init__(fields, description)


class SecurePublicationSchema(ManualSchema):
    def __init__(self):
        fields = [
            coreapi.Field(
                name="asset_id",
                required=True,
                location='path',
                schema=coreschema.String(
                    title="Asset ID",
                    description="Asset-id to retrieve"
                )
            )
        ]
        description = "Retrieve secure transaction (transfer to own secure deposit)"
        super().__init__(fields, description)


class TransferAssetSchema(ManualSchema):
    def __init__(self):
        fields = [
                    coreapi.Field(
                        name="asset_id",
                        required=True,
                        location='form',
                        schema=coreschema.String(
                            title="Asset ID",
                            description="Asset-id to transfer (secure-unlocker in case of secure-asset)"
                        )
                    ),
                    coreapi.Field(
                        name="crypto_id",
                        required=True,
                        location='form',
                        schema=coreschema.String(
                            title="Crypto ID",
                            description="Crypto ID of sender"
                        )
                    ),
                    coreapi.Field(
                        name="private_key",
                        required=True,
                        location='form',
                        schema=coreschema.String(
                            title="Private key",
                            description="Private key of crypto ID"
                        )
                    ),
                    coreapi.Field(
                        name="recipients",
                        required=True,
                        location='form',
                        schema=coreschema.String(
                            title="Recipients",
                            description="Recipient(s) of transaction"
                        )
                    )
        ]
        description = "Transfer asset from alias to recipients"
        super().__init__(fields, description)


class UserListSchema(AutoSchema):
    """
    Overrides `get_manual_fields()` to provide correct schema
    """
    def get_manual_fields(self, path, method):
        extra_fields = []
        if method == 'POST' or method == 'PUT':
            if method == 'POST':
                extra_fields = [
                    coreapi.Field(
                        name="username",
                        required=True,
                        location='form',
                        schema=coreschema.String(
                            title="Username",
                            description="Username"
                        )
                    ),
                    coreapi.Field(
                        name="password",
                        required=True,
                        location='form',
                        schema=coreschema.String(
                            title="Password",
                            description="New user password"
                        )
                    )
                ]

            if method == 'PUT':
                extra_fields = [
                    coreapi.Field(
                        name="password",
                        required=False,
                        location='form',
                        schema=coreschema.String(
                            title="Password",
                            description="New user password"
                        )
                    )
                ]

            extra_fields = extra_fields + [
                coreapi.Field(
                    name="is_staff",
                    required=False,
                    location='form',
                    schema=coreschema.Boolean(
                        title="Staff",
                        description="Staff status"
                    )
                ),
                coreapi.Field(
                    name="is_superuser",
                    required=False,
                    location='form',
                    schema=coreschema.Boolean(
                        title="Superuser",
                        description="Superuser status"
                    )
                ),
                coreapi.Field(
                    name="is_active",
                    required=False,
                    location='form',
                    schema=coreschema.Boolean(
                        title="Active",
                        description="Active status"
                    )
                ),
                coreapi.Field(
                    name="first_name",
                    required=False,
                    location='form',
                    schema=coreschema.String(
                        title="First name",
                        description="First name"
                    )
                ),
                coreapi.Field(
                    name="last_name",
                    required=False,
                    location='form',
                    schema=coreschema.String(
                        title="Last name",
                        description="Last name"
                    )
                ),
                coreapi.Field(
                    name="email",
                    required=False,
                    location='form',
                    schema=coreschema.String(
                        title="E-mail",
                        description="E-mail address"
                    )
                ),
            ]

        manual_fields = super().get_manual_fields(path, method)
        return manual_fields + extra_fields


class AssetBalanceSchema(ManualSchema):
    def __init__(self):
        fields = [
             coreapi.Field(
                 name="asset_id",
                 required=True,
                 location='path',
                 schema=coreschema.String(
                     title="Asset ID",
                     description="Asset-id to transfer (secure-unlocker in case of secure-asset)"
                 )
             ),
             coreapi.Field(
                 name="crypto_id",
                 required=False,
                 location='path',
                 schema=coreschema.String(
                     title="Crypto id",
                     description="Whose balance to check"
                 )
             )
        ]
        description = "Returns balance of given asset_id"
        super().__init__(fields, description)


class CoreAssetsSchema(AutoSchema):
    """
    Overrides `get_manual_fields()` to provide correct schema
    """

    def get_manual_fields(self, path, method):
        extra_fields = []
        if method == 'GET':
            extra_fields = []
        if method == 'POST':
            extra_fields = [
                coreapi.Field(
                    name="alias",
                    required=True,
                    location='form',
                    schema=coreschema.String(
                        title="Asset alias",
                        description="Alias of asset"
                    )
                ),
                coreapi.Field(
                    name="asset_id",
                    required=True,
                    location='form',
                    schema=coreschema.String(
                        title="ID of asset",
                        description="ID of asset"
                    )
                )
            ]

        manual_fields = super().get_manual_fields(path, method)
        return manual_fields + extra_fields
